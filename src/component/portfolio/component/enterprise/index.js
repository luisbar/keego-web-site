/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../../../view/basisComponent.js';
/**
 * It renders a component in order to show the name
 * of an enterprise and its logo
 */
class Enterprise extends BasisComponent {
  
  render() {

    return (
      <Box
        style={style.container}
        align={'center'}>

        <Box
          style={style.enterpriseName}>
          {this.props.enterpriseName}
        </Box>

        <Image
          style={style.enterpriseLogo}
          fit={'contain'}
          src={this.props.enterpriseLogo}/>
      </Box>
    );
  }
}

Enterprise.propTypes = {
  enterpriseName: PropTypes.string.isRequired,
  enterpriseLogo: PropTypes.string.isRequired,
};

export default Enterprise;