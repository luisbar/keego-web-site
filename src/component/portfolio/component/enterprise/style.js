export default {
  
  container: {
    height: '20vh',
    maxHeight: '20vh',
    marginTop: 30,
  },
  enterpriseName: {
    height: '25%',
    minHeight: '25%',
    cursor: 'default',
  },
  enterpriseLogo: {
    height: '75%',
    minHeight: '75%',
  },
};