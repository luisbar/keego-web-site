export default function(active) {

  return {

    container: {
      borderWidth: 0,
      borderBottomWidth: 1.5,
      borderStyle: 'solid',
      borderColor: active ? '#B3CA21' : '#A9A9A9',
      margin: 5,
      cursor: 'pointer',
      boxShadow: '0 0 0 0',
    },
  };
};
