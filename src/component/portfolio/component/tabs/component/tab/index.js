/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Heading from 'grommet/components/Heading';
import PropTypes from 'prop-types';
import { Animated } from 'react-animated-css';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../../../../../view/basisComponent.js';
/**
 * It renders the tab component
 */
class Tab extends BasisComponent {

  constructor(props) {
    super(props);
    //Listeners
    this._onTabSelected = (id) => this.onTabSelected.bind(this, id);
  }

  render() {

    return (
      <Animated
        animationIn={'fadeInDown'}
        animationOut={'fadeOutUp'}
        isVisible={this.props.animationVisibility}>

        <Box
          style={style(this.props.active).container}
          onClick={this._onTabSelected(this.props.id)}>

          <Heading
            tag={'h5'}
            align={'center'}>
            {this.props.name}
          </Heading>
        </Box>
      </Animated>
    );
  }

  onTabSelected(id) {
    this.props.onTabSelected(id);
  }
}

Tab.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  onTabSelected: PropTypes.func.isRequired,
  animationVisibility: PropTypes.bool.isRequired,
};

export default Tab;
