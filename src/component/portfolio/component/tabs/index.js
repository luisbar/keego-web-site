/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import BasisComponent from '../../../../view/basisComponent.js';
import { Tab } from './component';
/**
 * It renders the tabs component
 */
class Tabs extends BasisComponent {

  constructor(props) {
    super(props);
    //State
    this.state = {
      tabSelected: 0,
    };
    //Listeners
    this._onTabSelected = this.onTabSelected.bind(this);
  }

  render() {

    return (
      <Box
        flex={false}>

        <Box
          direction={'row'}
          justify={'center'}
          responsive={false}>
          {this.renderTabs()}
        </Box>
        {this.renderContentByTabSelected()}
      </Box>
    );
  }

  renderTabs() {

    return this.props.tabs.map((item, index) =>
      <Tab
        key={index}
        id={index}
        active={this.state.tabSelected === index}
        name={item}
        onTabSelected={this._onTabSelected}
        animationVisibility={this.props.animationVisibility}/>
    );
  }

  renderContentByTabSelected() {
    //If there is more than one child
    if (Array.isArray(this.props.children))
      return this.props.children[this.state.tabSelected];
    else
      return this.props.children;
  }

  onTabSelected(tabSelected) {
    this.setState({ tabSelected });
  }
}

Tabs.propTypes = {
  tabs: PropTypes.array,
  animationVisibility: PropTypes.bool.isRequired,
};

Tabs.defaultProps = {
  tabs: [
    'Tab 1',
    'Tab 2',
  ],
};

export default Tabs;
