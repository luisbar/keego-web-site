export default function(boxShadow) {
  
  return {
    
    container: {
      marginTop: 5,
    },
    productContainer: {
      boxShadow: boxShadow,
      borderRadius: 10,
      transition: 'all 0.5s',
      position: 'relative',
      margin: 5,
      padding: 10,
    },
    productName: {
      margin: 0,
    },
  };
};