/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Label from 'grommet/components/Label';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import './style.css'; 
import style from './style';
import BasisComponent from '../../../../view/basisComponent.js';
/**
 * It renders a component in order to show all products
 * made and grouped by enterprise
 */
class Products extends BasisComponent {
  
  constructor(props) {
    super(props);
    //State
    this.state = {
      initialBoxShadow: '-2px 2px 5px 0 #424242',
    };
    //Listeners
    this._renderProducts = this.renderProducts.bind(this);
    this._onMouseEnter = (id) => this.onMouseEnter.bind(this, id);
    this._onMouseLeave = (id) => this.onMouseLeave.bind(this, id);
    this._onProductPressed= (enterpriseId, productId) => this.onProductPressed.bind(this, enterpriseId, productId);
  }

  render() {

    return (
      <Box
        style={style().container}
        direction={'row'}
        justify={'center'}
        align={'center'}
        wrap={!this.state.isSmartphone}>
        {this.props.products.map(this._renderProducts)}
      </Box>
    );
  }
  
  renderProducts(product, index) {
    
    return (
      <Box
        key={product.id}
        className={'product'}
        style={style(this.state[this.props.enterpriseId + product.id] || this.state.initialBoxShadow).productContainer}
        justify={'center'}
        onMouseEnter={this._onMouseEnter(this.props.enterpriseId + product.id)}
        onMouseLeave={this._onMouseLeave(this.props.enterpriseId + product.id)}
        onClick={this._onProductPressed(this.props.enterpriseId, product.id)}>
        
        <Label
          style={style().productName}
          align={'center'}>
          {product.name}
        </Label>
      </Box>
    );
  }
  
  onMouseEnter(id, event) {
    this.setState({
      [id]: '-5px 5px 20px 0 #424242',
    });
  }
  
  onMouseLeave(id, event) {
    this.setState({
      [id]: '-2px 2px 5px 0 #424242',
    });
  }
  
  onProductPressed(enterpriseId, productId, event) {
    this.props.onShowProductModal(enterpriseId, productId);
  }
}

Products.propTypes = {
  products: PropTypes.array,
  enterpriseId: PropTypes.number.isRequired,
  onShowProductModal: PropTypes.func.isRequired,
};

Products.defaultProps = {
  products: [],
};

export default Products;