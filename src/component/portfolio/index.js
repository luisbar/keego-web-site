/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import PropTypes from 'prop-types';
import { Animated } from 'react-animated-css';
import { PacmanLoader } from 'react-spinners';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../view/basisComponent.js';
import { TXT_39, TXT_40 } from '../../string';
import { ProductModal } from './view/';
import { Tabs, Enterprise, Products } from './component/';
/**
 * It renders the portfolio component
 */
class Portfolio extends BasisComponent {

  constructor(props) {
    super(props);
    //State
    this.state = {
      productModalIsVisible: false,
      enterpriseId: null,
      productId: null,
    };
    //Listeners
    this._onProductModalClosed = this.onProductModalClosed.bind(this);
    this._onProductModalShowed = this.onProductModalShowed.bind(this);
  }

  render() {
  
    return (
      <Tabs
        tabs={[TXT_39]}
        animationVisibility={this.props.animationVisibility}>

        <Box
          style={style.productsByEnterpriseContainer}
          flex={false}
          direction={'row'}
          justify={'around'}
          wrap={!this.state.isSmartphone}>
          {this.renderProductsByEnterprise()}
          {this.renderProductModal()}
        </Box>
      </Tabs>
    );
  }
  
  renderProductsByEnterprise() {
    //Products by enterprise are being fetching
    if (this.props.fetchProductsByEnterpriseIsBeingExecuted)
      return (
        <Box
          pad={'xlarge'}
          align={'center'}>
          
          <PacmanLoader
            size={15}
            margin={'-10px'}
            color={'#FFFFFF'}
            loading={true}/>
        </Box>
      );
    //Products by enterprise have been fetched
    else if (this.props.productsByEnterprise.length)
      return this.props.productsByEnterprise.map((enterprise) =>
        <Animated
          key={enterprise.id}
          animationIn={'zoomIn'}
          animationOut={'zoomOut'}
          isVisible={this.props.animationVisibility}>
          
          <Box
            style={style.enterpriseAndProductContainer}>
            
            <Enterprise
              enterpriseName={enterprise.name}
              enterpriseLogo={enterprise.logo}/>
            <Products
              products={enterprise.products}
              enterpriseId={enterprise.id}
              onShowProductModal={this._onProductModalShowed}/>
          </Box>
        </Animated>
      )
    //Products by enterprise have not been fetched
    else 
      return (
        <Box
          pad={'xlarge'}>
          {TXT_40}
        </Box>
      );
  }
  
  renderProductModal() {

    if (this.state.productModalIsVisible) {

        const productSelected = this.props.productsByEnterprise
        .filter((enterprise) => enterprise.id == this.state.enterpriseId)[0]
        .products
        .filter((product) => product.id == this.state.productId)[0];
        
        return (
          <ProductModal
            onClose={this._onProductModalClosed}
            images={productSelected.images}
            videos={productSelected.videos}
            softwareName={productSelected.name}
            softwareDetail={productSelected.description}
            link={productSelected.link && productSelected.link.href}
            linkLabel={productSelected.link && productSelected.link.label}/>
        );
    }
  }
  
  componentDidMount() {
    super.componentDidMount();
    this.props.executeFetchProductsByEnterprise();
  }
  
  onProductModalShowed(enterpriseId, productId) {
    this.setState({
      productModalIsVisible: true,
      enterpriseId: enterpriseId,
      productId: productId,
    });
  }
  
  onProductModalClosed() {
    this.setState({ productModalIsVisible: false });
  }
}

Portfolio.propTypes = {
  productsByEnterprise: PropTypes.array.isRequired,
  fetchProductsByEnterpriseIsBeingExecuted: PropTypes.bool.isRequired,
  statusOfFetchingProductsByEnterprise: PropTypes.object.isRequired,
  executeFetchProductsByEnterprise: PropTypes.func.isRequired,
  animationVisibility: PropTypes.bool.isRequired,
};

export default Portfolio;
