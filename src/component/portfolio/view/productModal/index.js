/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Layer from 'grommet/components/Layer';
import Box from 'grommet/components/Box';
import Close from 'grommet/components/icons/base/Close';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../../../view/basisComponent.js';
import { Card } from './component';
/**
 * It renders a modal for showing a product
 */
class ProductModal extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Listeners
    this._onClose = this.onClose.bind(this);
  }

  render() {

    return (
      <Layer
        closer={this.renderCloser()}
        align={'top'}>

        <Box
          full={'vertical'}>
          <Card
            images={this.props.images}
            videos={this.props.videos}
            softwareName={this.props.softwareName}
            softwareDetail={this.props.softwareDetail}
            link={this.props.link}
            linkLabel={this.props.linkLabel}/>
        </Box>
      </Layer>
    );
  }
  
  renderCloser() {
    
    return (
      <Close
        style={style.closeIcon}
        colorIndex={'accent-1'}
        onClick={this._onClose}/>
    );
  }
  
  onClose() {
    this.props.onClose();
  }
}

ProductModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  images: PropTypes.array,
  videos: PropTypes.array,
  softwareName: PropTypes.string,
  softwareDetail: PropTypes.string,
  link: PropTypes.string,
  linkLabel: PropTypes.string,
};

export default ProductModal;
