export default {
  
  container: {
    height: 'auto',
    width: '70%',
    alignSelf: 'center',
  },
  carouselContainer: {
    marginTop: 45,
  },
  softwareName: {
    marginTop: '2%',
  },
  softwareDetail: {
    maxWidth: '100%',
    textAlign: 'justify',
  },
};