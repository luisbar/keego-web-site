/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Paragraph from 'grommet/components/Paragraph';
import Carousel from 'grommet/components/Carousel';
import Video from 'grommet/components/Video';
import Anchor from 'grommet/components/Anchor';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
/******************
 * Project import *
 ******************/
import './style.css';
import style from './style';
import BasisComponent from '../../../../../../view/basisComponent.js';
/**
 * It renders a card view in order to be showed in
 * the product modal view
 */
class Card extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Global variables
    this.carouselSettings = {
      dots: true,
      dotsClass: 'dots',
      arrows: false,
      autoplay: false,
      speed: 500,//Transition speed
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    //Listeners
    this._renderImages = this.renderImages.bind(this);
    this._renderVideos = this.renderVideos.bind(this);
  }

  render() {

    return (
      <Box
        flex={false}
        style={style.container}>

        <Box
          style={style.carouselContainer}>
          <Slider
            {...this.carouselSettings}>
            {this.props.images.map(this._renderImages)}
            {this.props.videos.map(this._renderVideos)}
          </Slider>
        </Box>

        <Title
          style={style.softwareName}>
          {this.props.softwareName}
        </Title>

        <Paragraph
          style={style.softwareDetail}>
          {this.props.softwareDetail}
        </Paragraph>
        
        <Anchor
          href={this.props.link}
          label={this.props.linkLabel}
          target={'blank'}/>
      </Box>
    );
  }
  
  renderImages(src, index) {
    
    return (
      <Image key={index} src={src}/>
    );
  }
  
  renderVideos(src, index) {
    
    return (
      <Video fit={'contain'} key={index}>
        <source src={src}/>
      </Video>
    );
  }
}

Card.propTypes = {
  images: PropTypes.array,
  videos: PropTypes.array,
  softwareName: PropTypes.string,
  softwareDetail: PropTypes.string,
  link: PropTypes.string,
  linkLabel: PropTypes.string,
}

Card.defaultProps = {
  images: [],
  videos: [],
};

export default Card;
