export default {
  
  closeIcon: {
    right: 15,
    top: 15,
    position: 'absolute',
    cursor: 'pointer',
  },
};