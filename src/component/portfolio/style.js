export default {
  
  productsByEnterpriseContainer: {
    height: 'auto',
    minHeight: '93%',
    overflow: 'hidden',
  },
  enterpriseAndProductContainer: {
    overflow: 'hidden',
  },
};