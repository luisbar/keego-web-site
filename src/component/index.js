import Home from './home/index';
import About from './about/index';
import Team from './team/index';
import Portfolio from './portfolio/index';
import Contact from './contact/index';
import NotFound from './notFound/index';
import DesktopToolBar from './desktopToolBar/index';
import MobileToolBar from './mobileToolBar/index';
import MobileMenu from './mobileMenu/index';
import FacebookIcon from './facebookIcon/index';
import TwitterIcon from './twitterIcon/index';

export {
  Home,
  About,
  Team,
  Portfolio,
  Contact,
  NotFound,
  DesktopToolBar,
  MobileToolBar,
  MobileMenu,
  FacebookIcon,
  TwitterIcon,
};