/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import SVGIcon from 'grommet/components/SVGIcon';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../../../view/basisComponent.js';
/**
 * It renders the divider which is used by home component
 */
class Divider extends BasisComponent {

  render() {

    return (
      <SVGIcon
        style={style.svgContainer}
        viewBox={'0 0 1278 40'}
        version={'1.0'}
        type={'logo'}>
        
        <g
          transform={'translate(0.000000, -183.000000)'}>
          <rect
            fill={'#B3CA21'}
            x={'0'}
            y={'183'}
            width={'1278'}
            height={this.state.isSmartphone ? '60' : '20'}/>
        </g>
      </SVGIcon>
    );
  }
}

export default Divider;