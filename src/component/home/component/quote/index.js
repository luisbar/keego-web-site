/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Typing from 'react-typing-animation';
import Paragraph from 'grommet/components/Paragraph';
/******************
 * Project import *
 ******************/
import './style.css';
import style from './style';
import BasisComponent from '../../../../view/basisComponent.js';
import { QUOTES } from '../../../../string.js';
/**
* It renders quotes with typewriter animation
*/
class Quote extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Listeners
    this._onFinishedTyping = this.onFinishedTyping.bind(this);
    //State
    this.state = {
      quote: QUOTES[Math.floor(Math.random() * QUOTES.length)],
    };
  }

  render() {

    const quoteLength = this.state.quote.quote.length + this.state.quote.author.length;
    
    return (
      <Typing
        cursorClassName={'cursor'}
        loop={true}
        startDelay={500}
        onFinishedTyping={this._onFinishedTyping}>
        
        <Paragraph
          style={style.quote}
          size={'medium'}>
          {`${this.state.quote.quote} ${this.state.quote.author}`}
        </Paragraph>
        
        <Typing.Backspace
          count={quoteLength}
          delay={2000}/>
      </Typing>
    );
  }
  
  onFinishedTyping() {
    this.setState({ quote: QUOTES[Math.floor(Math.random() * QUOTES.length)] });
  }
}

export default Quote;