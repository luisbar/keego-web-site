import Divider from './divider/index.js';
import Quote from './quote/index.js';

export {
  Divider,
  Quote,
};

