/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Footer from 'grommet/components/Footer';
import Label from 'grommet/components/Label';
import Headline from 'grommet/components/Headline';
import { Animated } from 'react-animated-css';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../view/basisComponent.js';
import { TXT_2, TXT_3, TXT_4, GOOGLE_MAP, EMAIL_ADDRESS } from '../../string.js';
import { Divider, Quote } from './component/index';
/**
 * It renders the home component
 */
class Home extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Listeners
    this._onOpenGoogleMaps = this.onOpenGoogleMaps.bind(this);
    this._onEmailing = this.onEmailing.bind(this);
  }

  render() {

    return (
      <Box
        style={style.container}
        flex={true}>

        {this.renderContentBody()}
        {this.renderContentFooter()}
      </Box>
    );
  }
  
  renderContentBody() {
    
    return (
      <Box
        style={style.body}
        flex={true}>
        
        <Box
          style={style.sloganAndDividerContainer}
          justify={'end'}
          align={'center'}>

          {this.renderSlogan()}
          {this.renderDivider()}
        </Box>
        
        <Box
          style={style.quoteContainer}
          align={'center'}>

          {this.renderQuote()}
        </Box>
      </Box>
    );
  }
  
  renderSlogan() {
    
    return (
      <Animated
        animationIn={'fadeInUp'}
        animationOut={'fadeOutDown'}
        isVisible={this.props.animationVisibility}>

        <Headline
          style={style.slogan}>
          {TXT_2}
        </Headline>
      </Animated>
    );
  }
  
  renderDivider() {
    
    return (
      <Animated
        style={style.animatedContainerOfDivider}
        animationIn={'fadeInUp'}
        animationOut={'fadeOutDown'}
        isVisible={this.props.animationVisibility}>

        <Divider/>
      </Animated>
    );
  }
  
  renderQuote() {
    
    return (
      <Animated
        animationIn={'fadeInUp'}
        animationOut={'fadeOutDown'}
        isVisible={this.props.animationVisibility}>

        <Quote/>
      </Animated>
    );
  }
  
  renderContentFooter() {
    
    return (
      <Footer
        justify={'around'}
        wrap={true}>
        
        {this.renderLocation()}
        {this.renderEmail()}
      </Footer>
    );
  }
  
  renderLocation() {
    
    return (
      <Animated
        animationIn={'slideInLeft'}
        animationOut={'slideOutLeft'}
        isVisible={this.props.animationVisibility}>
        <Label
          style={style.label}
          onClick={this._onOpenGoogleMaps}>
          {TXT_3}
        </Label>
      </Animated>
    );
  }
  
  renderEmail() {
    
    return (
      <Animated
        animationIn={'slideInRight'}
        animationOut={'slideOutRight'}
        isVisible={this.props.animationVisibility}>
        <Label
          style={style.label}
          onClick={this._onEmailing}>
          {TXT_4}
        </Label>
      </Animated>
    );
  }
  
  onOpenGoogleMaps(event) {
    window.open(GOOGLE_MAP);
  }
  
  onEmailing(event) {
    window.location.href = `mailto:${EMAIL_ADDRESS}`;
  }
}

Home.propTypes = {
  animationVisibility: PropTypes.bool.isRequired,
};

export default Home;