export default {
  
  container: {
    overflow: 'hidden',
  },
  body: {
    overflow: 'hidden',
  },
  sloganAndDividerContainer: {
    maxWidth: '100%',
    height: '50%',
  },
  slogan: {
    textAlign: 'center',
    cursor: 'default',
  },
  animatedContainerOfDivider: {
    width: '70%',
    textAlign: 'center',
  },
  quoteContainer: {
    height: '50%',
  },
  label: {
    display: 'flex',
    fontFamily: 'Calculator',
    margin: 0,
    textAlign: 'center',
    marginBottom: '5%',
    cursor: 'pointer',
  },
};