/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Title from 'grommet/components/Title';
import PropTypes from 'prop-types';
import { Animated } from 'react-animated-css';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../view/basisComponent.js';
/**
 * It renders a toolbar for desktop devices, it contains
 * the name of the current view
 */
class DesktopToolBar extends BasisComponent {

  render() {

    return (
      <Box
        style={style.container}
        justify={'center'}>
        <Animated
          style={style.animated}
          animationIn={'slideInLeft'}
          animationOut={'slideOutLeft'}
          isVisible={this.props.animationVisibility}>
          
          <Title
            style={style.currentViewName}>
            {this.props.currentViewName}
          </Title>
        </Animated>
      </Box>
    );
  }
}

DesktopToolBar.propTypes = {
  currentViewName: PropTypes.string.isRequired,
  animationVisibility: PropTypes.bool.isRequired,
};

export default DesktopToolBar;