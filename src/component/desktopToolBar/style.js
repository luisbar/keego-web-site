export default {
  
  container: {
    minHeight: '7%',
    maxHeight: '7%',
    height: '7%',
  },
  animated: {
    width: '100%',
  },
  currentViewName: {
    fontFamily: 'Calculator',
    marginLeft: '3%',
    cursor: 'default',
  },
};