/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import Box from 'grommet/components/Box';
import Title from 'grommet/components/Title';
/******************
 * Project import *
 ******************/
import BasisComponent from '../../view/basisComponent.js';
/**
 * It renders a component for displaying when
 * the resource requested have not been found
 */
class NotFound extends BasisComponent {

  render() {

    return (
      <Box
        flex={true}>
        {this.renderContentBody()}
      </Box>
    );
  }
  
  renderContentBody() {
    
    return (
      <Box
        flex={true}
        justify={'center'}
        align={'center'}>
        
        <Title>
          {'No se encontro el recurso :('}
        </Title>
      </Box>
    );
  }
}

export default NotFound;