export default function(isSmartphone, isFullScreen) {
  
  return {
    
    container: {
      overflow: 'hidden',
      height: 'auto',
      minHeight: '93%',
    },
    worldMapContainer: {
      overflow: 'hidden',
    },
    worldMapMessage: {
      borderRadius: 5,
      fontSize: 12,
      padding: 5
    },
    mapContainer: {
      overflow: 'hidden',
      zIndex: 3,
      borderRadius: !isFullScreen && '100%',
      position: isFullScreen && 'fixed',
      top: isFullScreen && 0,
      right: isFullScreen && 0,
      bottom: isFullScreen && 0,
      left: isFullScreen && 0,
      border: !isFullScreen && '2px solid #B3CA21',
      width: isFullScreen
        ? Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
        : Math.max(document.documentElement.clientHeight, window.innerHeight || 0) / (isSmartphone ? 2 : 3.1),
      height: !isFullScreen && Math.max(document.documentElement.clientHeight, window.innerHeight || 0) / (isSmartphone ? 2 : 3.1),
    },
    emailAndPhoneNumbersContainer: {
      width: isSmartphone ? '100%' : '40%',
    },
    emailAndPhoneNumbersAnimatedContainer: {
      display: 'flex',
      flexDirection: 'column'
    },
  };
};