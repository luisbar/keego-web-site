export default function (isSmartphone, sendEmailIsBeingExecuted) {
  
  return {
    
    formContainer: {
      width: isSmartphone ? '100%' : '40%',
    },
    formField: {
      paddingLeft: 5,
    },
    formFieldLabel: {
      color: '#424242',
    },
    formFieldError: {
      color: '#F04953',
      position: 'absolute',
      left: 5,
      top: 20,
      width: '95%',
    },
    form: {
      width: '100%',
    },
    textArea: {
      color: '#424242',
      resize: 'none',
    },
    textInput: {
      color: '#424242',
    },
    formButton: {
      backgroundColor: '#B3CA21',
      borderColor: '#B3CA21',
      textAlign: 'center',
      height: sendEmailIsBeingExecuted ? 70 : 45,
      transition: 'height 0.5s',
    },
  };
};