/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Form from 'grommet/components/Form';
import Label from 'grommet/components/Label';
import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import FormField from 'grommet/components/FormField';
import Heading from 'grommet/components/Heading';
import TextInput from 'grommet/components/TextInput';
import Footer from 'grommet/components/Footer';
import Button from 'grommet/components/Button';
import Checkmark from 'grommet/components/icons/base/Checkmark';
import { PacmanLoader } from 'react-spinners';
import { Animated } from 'react-animated-css';
import isEmail from 'validator/lib/isEmail';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../../../view/basisComponent.js';
import {
  TXT_25,
  TXT_26,
  TXT_27,
  TXT_28,
  TXT_29,
  TXT_30,
  TXT_35,
  TXT_36,
  TXT_37,
  TXT_38,
} from '../../../../string';
/**
 * It renders the contact form component
 */
class ContactForm extends BasisComponent {

  constructor(props) {
    super(props);
    //State
    this.state = {
      errorOnInputName: null,
      errorOnInputEmail: null,
      errorOnInputSubject: null,
      errorOnInputMessage: null,
      emailSent: false,
      name: '',
      email: '',
      subject: '',
      message: '',
    };
    //Listeners
    this._onSubmit = this.onSubmit.bind(this);
    this._onTextChange = (label) => this.onTextChange.bind(this, label);
  }

  componentWillReceiveProps(nextProps) {
    //If sendEmail process has been executed completely
    if (this.props.sendEmailIsBeingExecuted &&
        !nextProps.sendEmailIsBeingExecuted)
      this.setState({
        emailSent: !this.props.statusOfSendingEmail.wasError,
      });
  }

  render() {

    return (
      <Box
        style={style(this.state.isSmartphone).formContainer}
        pad={'small'}>

        <Animated
          animationIn={'fadeInRight'}
          animationOut={'fadeOutRight'}
          isVisible={this.props.animationVisibility}>
          <Form
            style={style().form}
            onSubmit={this._onSubmit}>
            {this.renderFormHeader()}
            {this.renderFormField(TXT_25, false, this.state.errorOnInputName)}
            {this.renderFormField(TXT_26, false, this.state.errorOnInputEmail)}
            {this.renderFormField(TXT_27, false, this.state.errorOnInputSubject)}
            {this.renderFormField(TXT_28, true, this.state.errorOnInputMessage)}
            {this.renderFormFooter()}
          </Form>
        </Animated>
      </Box>
    );
  }

  renderFormHeader() {

    return (
      <Header>
        <Title>
          {TXT_29}
        </Title>
      </Header>
    );
  }

  renderFormField(label, multiline, error) {

    return (
      <FormField
        style={style().formField}
        error={error}
        label={this.renderFormFieldLabel(label)}>
        {
          multiline
          ? this.renderTextArea(label)
          : this.renderTextInput(label)
        }
      </FormField>
    );
  }

  renderFormFieldLabel(label) {

    return (
      <Heading
        style={style().formFieldLabel}
        tag={'h5'}>
        {label}
      </Heading>
    );
  }

  renderFormFieldError(error) {

    return (
      <Label
        style={style().formFieldError}
        truncate={true}
        size={'small'}>
        {error}
      </Label>
    );
  }

  renderTextArea(label) {

    return (
      <textarea
        style={style().textArea}
        onChange={this._onTextChange(label)}
        value={this.state[label === TXT_25
          ? 'name'
          : label === TXT_26
          ? 'email'
          : label === TXT_27
          ? 'subject'
          : label === TXT_28
          ? 'message'
          : ''
        ]}/>
    );
  }

  renderTextInput(label) {

    return (
      <TextInput
          style={style().textInput}
          onDOMChange={this._onTextChange(label)}
          value={this.state[label === TXT_25
            ? 'name'
            : label === TXT_26
            ? 'email'
            : label === TXT_27
            ? 'subject'
            : label === TXT_28
            ? 'message'
            : ''
          ]}/>
    );
  }

  renderFormFooter() {

    return (
      <Footer>
        <Button
          style={style(null, this.props.sendEmailIsBeingExecuted).formButton}
          type={'submit'}
          label={!this.props.sendEmailIsBeingExecuted && !this.state.emailSent && TXT_30}
          primary={true}
          fill={true}
          icon={this.renderButtonIcon()}/>
      </Footer>
    );
  }

  renderButtonIcon() {

    return (
      this.props.sendEmailIsBeingExecuted
      ? <PacmanLoader
          size={15}
          margin={'-10px'}
          color={'#FFFFFF'}
          loading={true}/>
      : this.state.emailSent
      ? <Checkmark
          colorIndex={'neutral-5'}/>
      : null
    );
  }

  onSubmit(event) {
    event.preventDefault();
    //To show errors on formFields
    this.setState({
      errorOnInputName: !this.state.name &&
        this.renderFormFieldError(TXT_35),
      errorOnInputEmail: (!this.state.email || !isEmail(this.state.email)) &&
        this.renderFormFieldError(TXT_36),
      errorOnInputSubject: !this.state.subject &&
        this.renderFormFieldError(TXT_37),
      errorOnInputMessage: !this.state.message &&
        this.renderFormFieldError(TXT_38),
    })
    //To execute the recaptcha
    if (this.state.name &&
        this.state.email &&
        this.state.subject &&
        this.state.message &&
        isEmail(this.state.email))
      this.props.recaptcha.execute();
    //To reset the recaptcha and to clear all text inputs
    if (this.state.emailSent) {
      this.setState({
        emailSent: false,
        name: '',
        email: '',
        subject: '',
        message: '',
      });
      this.props.recaptcha.reset();
    }
  }

  onVerifyRecaptcha(token) {
    this.props.executeSendEmail(
      this.state.name,
      this.state.email,
      this.state.subject,
      this.state.message,
      token
    );
  }

  onTextChange(id, event) {

    switch (id) {

      case TXT_25:
        this.setState({ name: event.target.value });
        break;
      case TXT_26:
        this.setState({ email: event.target.value });
        break;
      case TXT_27:
        this.setState({ subject: event.target.value });
        break;
      case TXT_28:
        this.setState({ message: event.target.value });
        break;
    }
  }
}

ContactForm.propTypes = {
  recaptcha: PropTypes.any,
  animationVisibility: PropTypes.bool.isRequired,
  executeSendEmail: PropTypes.func.isRequired,
  sendEmailIsBeingExecuted: PropTypes.bool.isRequired,
  statusOfSendingEmail: PropTypes.object.isRequired,
};

export default ContactForm;
