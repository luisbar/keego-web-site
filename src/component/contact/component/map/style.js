export default {
  
  loadingElement: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  containerElement: {
    width: '100%',
    height: '100%',
  },
  mapElement: {
    width: '100%',
    height: '100%',
  },
};