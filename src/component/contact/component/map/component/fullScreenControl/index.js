/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import SVGIcon from 'grommet/components/SVGIcon';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../../../../../view/basisComponent.js'
/**
 * It renders the fullScreen control which will be rendered on map component
 */
class FullScreenControl extends BasisComponent {

  render() {

    return (
      <Box
        style={style.container}>

        <SVGIcon
          style={style.svgContainer}
          viewBox={`-4 0 32 24`}
          version={'1.0'}
          type={'logo'}
          onClick={this.props.onClick}>
          {
            this.props.isFullScreen
            ? <path
                fill={'#B3CA21'}
                d={'M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z'}>
              </path>
            : <path
                fill={'#B3CA21'}
                d={'M5,5H10V7H7V10H5V5M14,5H19V10H17V7H14V5M17,14H19V19H14V17H17V14M10,17V19H5V14H7V17H10Z'}>
              </path>
          }
        </SVGIcon>
      </Box>
    );
  }
}

FullScreenControl.propTypes = {
  onClick: PropTypes.func.isRequired,
  isFullScreen: PropTypes.bool.isRequired,//If true a close icon is shown if not the full screen icon is shown
};

export default FullScreenControl;
