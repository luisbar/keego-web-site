export default {
  
  container: {
    textAlign: 'center',
    borderRadius: '100%',
    border: '2px solid #B3CA21',
    backgroundColor: '#424242',
    marginTop: 10,
  },
  svgContainer: {
    cursor: 'pointer',
    width: 24,
    height: 24
  },
};