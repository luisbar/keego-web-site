/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import PropTypes from 'prop-types';
import { MAP } from 'react-google-maps/lib/constants';
/******************
 * Project import *
 ******************/
import BasisComponent from '../../../../../../view/basisComponent.js'
/**
 * It renders the container that contains the map controls
 */
class ControlContainer extends BasisComponent {

  render() {
    return <div ref={'div'}>{this.props.children}</div>;
  }

  componentDidMount() {
    super.componentDidMount();
    if (this.props.map && !this.map) {
      this.map = this.props.map.context;
      this.map[MAP].controls[this.props.position].push(this.refs.div);
    }
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    if (this.map)
      this.map[MAP].controls[this.props.position].pop();
  }
}

ControlContainer.propTypes = {
  map: PropTypes.any.isRequired,
  position: PropTypes.any.isRequired,
};

export default ControlContainer;