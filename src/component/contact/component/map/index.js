/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import PropTypes from 'prop-types';
import { compose, withProps } from 'recompose';
import { PacmanLoader } from 'react-spinners';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from 'react-google-maps';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../../../view/basisComponent.js';
import { GOOGLE_MAP_API } from '../../../../config';
import { LATITUDE, LONGITUDE } from '../../../../string';
import { FullScreenControl, ControlContainer, GoogleMapsControl } from './component/index';
/*************
 * Constants *
 *************/
const aubergineStyle = require('./style.json');
/**
 * It renders the map
 */
class Map extends BasisComponent {
  
  constructor(props) {
    super(props);
    //State
    this.state = {
      map: null,//To save a map reference
    };
    //Listeners
    this._onFullScreenPressed = this.onFullScreenPressed.bind(this);
  }

  render() {

    return (
      <GoogleMap
        ref={'map'}
        defaultZoom={16}
        options={{
          zoomControl: false,
          mapTypeControl: false,
          scaleControl: false,
          streetViewControl: false,
          rotateControl: false,
          fullscreenControl: false,
          styles: aubergineStyle
        }}
        defaultCenter={{
          lat: LATITUDE,
          lng: LONGITUDE,
        }}>
        
        <Marker
          position={{
            lat: LATITUDE,
            lng: LONGITUDE,
          }}/>

        {
          this.state.map &&
          <ControlContainer
            map={this.state.map}
            position={window.google.maps.ControlPosition.TOP_CENTER}>
            
            <FullScreenControl
              onClick={this._onFullScreenPressed}
              isFullScreen={this.props.isFullScreen}/>
            <GoogleMapsControl/>
          </ControlContainer>
        }
      </GoogleMap>
    );
  }
  
  componentDidMount() {
    super.componentDidMount();
    this.setState({ map: this.refs.map })
  }
  
  onFullScreenPressed(event) {
    this.props.onFullScreenPressed()
  }
}

Map.propTypes = {
  onFullScreenPressed: PropTypes.func.isRequired,
  isFullScreen: PropTypes.bool.isRequired,
};

const MyMapComponent = compose(
  withProps({
    googleMapURL: GOOGLE_MAP_API,
    loadingElement: <div style={style.loadingElement}>
                      <PacmanLoader
                          size={15}
                          margin={'-10px'}
                          color={'#765AEF'}
                          loading={true}/>
                    </div>,
    containerElement: <div style={style.containerElement}/>,
    mapElement: <div style={style.mapElement}/>
  }),
  withScriptjs,
  withGoogleMap
)(Map);

export default MyMapComponent;