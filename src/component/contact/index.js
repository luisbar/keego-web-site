/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import WorldMap from 'grommet/components/WorldMap';
import Label from 'grommet/components/Label';
import Toast from 'grommet/components/Toast';
import { Animated } from 'react-animated-css';
import PropTypes from 'prop-types';
import Recaptcha from 'react-google-invisible-recaptcha';
/******************
 * Project import *
 ******************/
import './style.css';
import style from './style.js';
import BasisComponent from '../../view/basisComponent.js';
import { Map, ContactForm } from './component/index';
import { RECAPTCHA_SITE_KEY } from '../../config';
import {
  TXT_31,
  TXT_32,
  TXT_33,
  TXT_34,
  EMAIL_ADDRESS,
  DEIBY_PHONE,
  LUIS_PHONE,
} from '../../string';
/**
 * It renders the contact component
 */
class Contact extends BasisComponent {

  constructor(props) {
    super(props);
    //State
    this.state = {
      isFullScreen: false,
      errorMessage: '',
    };
    //Listeners
    this._onFullScreenPressed = this.onFullScreenPressed.bind(this);
    this._onToastClosed = this.onToastClosed.bind(this);
    this._onVerifyRecaptcha = this.onVerifyRecaptcha.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    //If sendEmail process has been executed completely and was an error
    if (this.props.sendEmailIsBeingExecuted &&
        !nextProps.sendEmailIsBeingExecuted &&
        this.props.statusOfSendingEmail.wasError) {
      this.setState({
        errorMessage: this.props.statusOfSendingEmail.error.errorMessage,
      });
      this.refs.recaptcha.reset();
    }
  }

  render() {

    return (
      <Box
        style={style().container}
        flex={false}>
        {this.renderTopSide()}
        {this.renderBottomSide()}
        {this.renderRecaptcha()}
        {this.renderToast()}
      </Box>
    );
  }

  renderTopSide() {

    return (
      <Box
        align={'center'}
        direction={'row'}>
        {this.renderWorldMap()}
        <ContactForm
          ref={'contactForm'}
          recaptcha={this.refs.recaptcha}
          animationVisibility={this.props.animationVisibility}
          executeSendEmail={this.props.executeSendEmail}
          sendEmailIsBeingExecuted={this.props.sendEmailIsBeingExecuted}
          statusOfSendingEmail={this.props.statusOfSendingEmail}/>
      </Box>
    );
  }

  renderWorldMap() {

    return (
      <Box
        style={style().worldMapContainer}
        flex={true}>

        <Animated
          animationIn={'fadeInLeft'}
          animationOut={'fadeOutLeft'}
          isVisible={this.props.animationVisibility}>
          <WorldMap
            series={this.getContinents()}/>
        </Animated>
      </Box>
    );
  }

  renderBottomSide() {

    return (
      <Box
        flex={true}
        direction={'row'}
        reverse={this.state.isSmartphone}>
        {this.renderMap()}
        {this.renderEmailAndPhoneNumbers()}
      </Box>
    );
  }

  renderMap() {

    return (
      <Box
        flex={true}
        align={'center'}
        justify={'center'}>

        <Animated
          style={style(this.state.isSmartphone, this.state.isFullScreen).mapContainer}
          animationIn={'zoomIn'}
          animationOut={'zoomOut'}
          isVisible={this.props.animationVisibility}>
          <Map
            onFullScreenPressed={this._onFullScreenPressed}
            isFullScreen={this.state.isFullScreen}/>
        </Animated>
      </Box>
    );
  }

  renderEmailAndPhoneNumbers() {

    return (
      <Box
        style={style(this.state.isSmartphone).emailAndPhoneNumbersContainer}
        pad={'small'}>

        <Animated
          style={style().emailAndPhoneNumbersAnimatedContainer}
          animationIn={'zoomIn'}
          animationOut={'zoomOut'}
          isVisible={this.props.animationVisibility}>
          <Label
            size={'small'}>
            {TXT_31}
            <a className={'a'} href={`tel:591${DEIBY_PHONE}`}>{DEIBY_PHONE}</a>
            {TXT_32}
            <a className={'a'} href={`tel:591${LUIS_PHONE}`}>{LUIS_PHONE}</a>
          </Label>
          <Label
            size={'small'}>
            <a className={'a'} href={`mailto:${EMAIL_ADDRESS}`}>{EMAIL_ADDRESS}</a>
          </Label>
        </Animated>
      </Box>
    );
  }

  renderRecaptcha() {

    return (
      <Recaptcha
        ref={'recaptcha'}
        sitekey={RECAPTCHA_SITE_KEY}
        onResolved={this._onVerifyRecaptcha}/>
    );
  }

  renderToast() {

    return (
      this.state.errorMessage &&
      <Toast
        status={'critical'}
        onClose={this._onToastClosed}>
        {this.state.errorMessage}
      </Toast>
    );
  }

  onFullScreenPressed() {
    this.setState({ isFullScreen: !this.state.isFullScreen });
  }

  onToastClosed() {
    this.setState({ errorMessage: '' });
  }

  onVerifyRecaptcha(token) {
    this.refs.contactForm.onVerifyRecaptcha(token);
  }

  getContinents() {

    return [
      {
        continent: 'NorthAmerica',
        colorIndex: 'accent-4',
      },
      {
        continent: 'SouthAmerica',
        colorIndex: 'accent-2',
        label: TXT_33,
        flag: <Box
                style={style().worldMapMessage}
                colorIndex={'accent-8'}>
                {TXT_34}
              </Box>,
      },
      {
        continent: 'Europe',
        colorIndex: 'accent-1',
      },
      {
        continent: 'Africa',
        colorIndex: 'accent-5',
      },
      {
        continent: 'Asia',
        colorIndex: 'accent-6',
      },
      {
        continent: 'Australia',
        colorIndex: 'accent-7',
      },
    ];
  }
}

Contact.propTypes = {
  animationVisibility: PropTypes.bool.isRequired,
  executeSendEmail: PropTypes.func.isRequired,
  statusOfSendingEmail: PropTypes.object.isRequired,
  sendEmailIsBeingExecuted: PropTypes.bool.isRequired,
};

export default Contact;
