export default function(isSmartphone) {
  
  return {
    
    container: {
      height: 'auto',
      minHeight: '93%',
      overflow: 'hidden',
    },
    teamMemberContainer: {
      overflow: 'hidden',
    },
    pictureContainer: {
      overflow: 'hidden',
      height: isSmartphone && '50%',
      minHeight: isSmartphone && '50%',
      maxHeight: isSmartphone && '50%',
    },
    animatedContainerOfPicture: {
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    picture: {
      borderRadius: '100%',
      border: '2px solid #B3CA21',
      width: '50%',
    },
    detailContainer: {
      overflow: 'hidden',
      height: isSmartphone && '50%',
      minHeight: isSmartphone && '50%',
      maxHeight: isSmartphone && '50%',
    },
    animatedContainerOfDetail: {
      width: '100%',
      height: 'auto',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
    },
    name: {
      textAlign: 'center',
    },
  };
};