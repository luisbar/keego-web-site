/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Label from 'grommet/components/Label';
import Paragraph from 'grommet/components/Paragraph';
import { Animated } from 'react-animated-css';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../view/basisComponent.js';
import FacebookIcon from '../facebookIcon/index';
import TwitterIcon from '../twitterIcon/index';
import WhatsappIcon from '../whatsappIcon/index';
import {
  TXT_19,
  TXT_20,
  TXT_21,
  TXT_22,
  TXT_23,
  TXT_24,
  LUIS_FACEBOOK,
  LUIS_TWITTER,
  LUIS_WHATSAPP,
  DEIBY_FACEBOOK,
  DEIBY_TWITTER,
  DEIBY_WHATSAPP,
} from '../../string';
/**
 * It renders the team component
 */
class Team extends BasisComponent {

  render() {

    return (
      <Box
        style={style().container}
        flex={false}>
        {this.renderTeamMember(
          'https://api.adorable.io/avatars/285/deibyloayza@adorable.io.png',
          TXT_19,
          TXT_20,
          TXT_21,
          false,
          DEIBY_FACEBOOK,
          DEIBY_TWITTER,
          DEIBY_WHATSAPP
        )}
        {this.renderTeamMember(
          'https://api.adorable.io/avatars/285/luisbarrancos@adorable.io.png',
          TXT_22,
          TXT_23,
          TXT_24,
          true,
          LUIS_FACEBOOK,
          LUIS_TWITTER,
          LUIS_WHATSAPP
        )}
      </Box>
    );
  }
  
  renderTeamMember(picture, name, position, description, reverse, facebook, twitter, whatsapp) {
    
    return (
      <Box
        style={style().teamMemberContainer}
        flex={true}
        direction={'row'}
        reverse={reverse && this.state.isSmartphone}>
        {
          reverse
          ? this.renderDetail(name, position, description, facebook, twitter, whatsapp)
          : this.renderPicture(picture, reverse)
        }
        {
          reverse
          ? this.renderPicture(picture, reverse)
          : this.renderDetail(name, position, description, facebook, twitter, whatsapp)
        }
      </Box>
    );
  }
  
  renderPicture(picture, reverse) {
    
    return (
      <Box
        style={style().pictureContainer}
        flex={true}>

        <Animated
          style={style().animatedContainerOfPicture}
          animationIn={reverse ? 'fadeInRight' : 'fadeInLeft'}
          animationOut={reverse ? 'fadeOutRight' : 'fadeOutLeft'}
          isVisible={this.props.animationVisibility}>

          <Image
            style={style().picture}
            src={picture}/>
        </Animated>
      </Box>
    );
  }
  
  renderDetail(name, position, description, facebook, twitter, whatsapp) {
    
    return (
      <Box
        style={style().detailContainer}
        flex={true}
        justify={'center'}>
        
        {this.renderNamePositionAndDescription(name, position, description)}
        {this.renderSocialNetworkIcons(facebook, twitter, whatsapp)}
      </Box>
    );
  }
  
  renderNamePositionAndDescription(name, position, description) {
    
    return (
      <Animated
        style={style().animatedContainerOfDetail}
        animationIn={'zoomIn'}
        animationOut={'zoomOut'}
        isVisible={this.props.animationVisibility}>

        <Title
          style={style().name}
          truncate={false}>
          {name}
        </Title>
        
        <Label
          align={'center'}>
          {position}
        </Label>
        
        <Paragraph
          align={'center'}
          margin={'none'}>
          {description}
        </Paragraph>
      </Animated>
    );
  }
  
  renderSocialNetworkIcons(facebook, twitter, whatsapp) {
    
    return (
      <Box
        direction={'row'}
        responsive={false}
        justify={'center'}>
        
        <Animated
          animationIn={'bounceIn'}
          animationOut={'zoomOut'}
          animationInDelay={500}
          isVisible={this.props.animationVisibility}>

          <FacebookIcon
            primaryIconColor={'#FFFFFF'}
            secondaryIconColor={'#FFFFFF'}
            url={facebook}
            size={50}/>
        </Animated>
        
        <Animated
          animationIn={'bounceIn'}
          animationOut={'zoomOut'}
          animationInDelay={1000}
          isVisible={this.props.animationVisibility}>

          <TwitterIcon
            primaryIconColor={'#FFFFFF'}
            secondaryIconColor={'#FFFFFF'}
            url={twitter}
            size={50}/>
        </Animated>
        
        <Animated
          animationIn={'bounceIn'}
          animationOut={'zoomOut'}
          animationInDelay={1500}
          isVisible={this.props.animationVisibility}>

          <WhatsappIcon
            primaryIconColor={'#FFFFFF'}
            secondaryIconColor={'#FFFFFF'}
            url={whatsapp}
            size={50}/>
        </Animated>
      </Box>
    );
  }
}

export default Team;