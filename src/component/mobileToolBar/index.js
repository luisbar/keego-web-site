/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import Menu from 'grommet/components/icons/base/Menu';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../view/basisComponent.js';
/**
 * It renders a toolbar for mobile devices, it contains
 * the name of the current view and a hamburger menu button
 */
class MobileToolBar extends BasisComponent {

  render() {

    return (
      <Header
        colorIndex={'grey-3'}
        justify={'between'}>

        <Title
          style={style.currentViewName}>
          {this.props.currentViewName}
        </Title>
        
        <Menu
          style={style.menuIcon}
          onClick={this.props.onHamburguerMenuButtonPressed}/>
      </Header>
    );
  }
}

MobileToolBar.propTypes = {
  currentViewName: PropTypes.string.isRequired,
  onHamburguerMenuButtonPressed: PropTypes.func.isRequired,
};

export default MobileToolBar;