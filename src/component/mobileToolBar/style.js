export default {
  
  currentViewName: {
    fontFamily: 'Calculator',
    marginLeft: '3%',
  },
  menuIcon: {
    marginRight: '5%',
    cursor: 'pointer',
  },
};