/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Image from 'grommet/components/Image';
import Heading from 'grommet/components/Heading';
import Close from 'grommet/components/icons/base/Close';
import { Animated } from 'react-animated-css';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import './style.css';
import style from './style';
import BasisComponent from '../../view/basisComponent.js';
import {
  TXT_5,
  TXT_6,
  TXT_7,
  TXT_8,
  TXT_9,
} from '../../string.js';
import {
  HOME,
  ABOUT,
  TEAM,
  PORTFOLIO,
  CONTACT,
} from '../../config.js';
/**
 * It renders a menu in order to be used by mobile devices
 */
class MobileMenu extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Global variables
    this.normalAnimation = 'normalAnimation';
    this.fastAnimation = 'fastAnimation';
    //Listeners
    this._onCloseMenu = (viewId) => this.onCloseMenu.bind(this, viewId);
  }

  render() {
    const flag = this.animated && this.animated.className.split(' ')[this.animated.className.split(' ').length - 1];

    return (
      <Animated
        innerRef={(animated) => this.animated = animated}
        className={this.props.menuVisibility || (!this.props.menuVisibility && flag === this.normalAnimation)
          ? this.normalAnimation
          : this.fastAnimation
        }
        animationIn={'bounceInDown'}
        animationOut={'bounceOutUp'}
        isVisible={this.props.menuVisibility}>
        
        <Box
          full={true}
          colorIndex={'grey-3'}>
          {this.renderMenuHeader()}
          {this.renderMenuBody()}
        </Box>
      </Animated>
    );
  }
  
  renderMenuHeader() {
    
    return (
      <Header
        justify={'between'}
        align={'start'}>
        
        <Close
          style={style.auxiliarIcon}/>
        
        <Box>
          <Image
            style={style.logo}
            src={'./resource/image/mobile-logo.svg'}
            size={'small'}/>
            
          <Image
            src={'./resource/image/mobile-enterprise-name.svg'}
            size={'small'}/>
        </Box>
        
        <Close
          style={style.closeIcon}
          onClick={this.props.onCloseButtonPressed}/>
      </Header>
    );
  }
  
  renderMenuBody() {
    
    return (
      <Box
        flex={true}
        justify={'center'}
        align={'center'}>
        {this.renderMenuItem(HOME, TXT_5)}
        {this.renderMenuItem(ABOUT, TXT_6)}
        {this.renderMenuItem(TEAM, TXT_7)}
        {this.renderMenuItem(PORTFOLIO, TXT_8)}
        {this.renderMenuItem(CONTACT, TXT_9)}
      </Box>
    );
  }
  
  renderMenuItem(viewId, title) {
    
    return (
      <Box
        onClick={this._onCloseMenu(viewId)}
        pad={'medium'}>
        <Heading
          tag={'h6'}>
          {title}
        </Heading>
      </Box>
    );
  }
  
  onCloseMenu(viewId, event) {
    if (viewId !== this.props.location.pathname)
      this.props.replace(viewId);
    
    this.props.onCloseButtonPressed()
  }
}

MobileMenu.propTypes = {
  replace: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  menuVisibility: PropTypes.bool.isRequired,
  onCloseButtonPressed: PropTypes.func.isRequired,
};

export default MobileMenu;