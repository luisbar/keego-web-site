export default {
  
  auxiliarIcon: {
    marginTop: '5%',
    marginLeft: '5%',
    cursor: 'none',
    opacity: 0,
  },
  logo: {
    marginTop: '5%',
  },
  closeIcon: {
    marginTop: '5%',
    marginRight: '5%',
    cursor: 'pointer',
  },
};