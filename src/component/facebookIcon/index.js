/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import SVGIcon from 'grommet/components/SVGIcon';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../../view/basisComponent.js'
/**
 * It renders the facebook icon
 */
class FacebookIcon extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Listeners
    this._onMouseEnter = this.onMouseEnter.bind(this);
    this._onMouseLeave = this.onMouseLeave.bind(this);
    this._onIconPressed = this.onIconPressed.bind(this);
    //State
    this.state = {
      fill: this.props.primaryIconColor,
    };
  }

  render() {

    return (
      <SVGIcon
        style={style.container}
        viewBox={`${35 - this.props.size} 0 ${this.props.size} 19`}
        version={'1.0'}
        type={'logo'}
        onMouseEnter={this._onMouseEnter}
        onMouseLeave={this._onMouseLeave}
        onClick={this._onIconPressed}>

        <path
          fill={this.state.fill}
          d={'M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M18,5H15.5A3.5,3.5 0 0,0 12,8.5V11H10V14H12V21H15V14H18V11H15V9A1,1 0 0,1 16,8H18V5Z'}>
        </path>
      </SVGIcon>
    );
  }
  
  onMouseEnter(event) {
    this.setState({
      fill: this.props.secondaryIconColor,
    });
  }
  
  onMouseLeave(event) {
    this.setState({
      fill: this.props.primaryIconColor,
    });
  }
  
  onIconPressed() {
    window.open(this.props.url);
  }
}

FacebookIcon.propTypes = {
  primaryIconColor: PropTypes.string.isRequired,
  secondaryIconColor: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  size: PropTypes.number,
};

FacebookIcon.defaultProps = {
  size: 35,
};

export default FacebookIcon;
