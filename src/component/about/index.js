/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Title from 'grommet/components/Title';
import Paragraph from 'grommet/components/Paragraph';
import ReactBubbleChart from 'react-bubble-chart';
import { Animated } from 'react-animated-css';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../view/basisComponent.js';
import { TXT_14, TXT_15, TXT_16, TXT_17, TXT_18, TECHNOLOGIES } from '../../string';
/*************
 * Constants *
 *************/
const colorLegend = [
  {color: '#574D68', textColor: '#FFFFFF'},
  {color: '#0EAD69', textColor: '#FFFFFF'},
  {color: '#17BEBB', textColor: '#FFFFFF'},
  {color: '#B0228C', textColor: '#FFFFFF'},
  {color: '#60A561', textColor: '#FFFFFF'},
  {color: '#A38560', textColor: '#FFFFFF'},
  {color: '#32965D', textColor: '#FFFFFF'},
  {color: '#7067CF', textColor: '#FFFFFF'},
  {color: '#330C2F', textColor: '#FFFFFF'},
  {color: '#F4989C', textColor: '#FFFFFF'},
  {color: '#DBABBE', textColor: '#FFFFFF'},
  {color: '#13315C', textColor: '#FFFFFF'},
  {color: '#134074', textColor: '#FFFFFF'},
  {color: '#797B84', textColor: '#FFFFFF'},
  {color: '#9C0D38', textColor: '#FFFFFF'},
  {color: '#F64740', textColor: '#FFFFFF'},
  {color: '#2A324B', textColor: '#FFFFFF'},
  {color: '#767B91', textColor: '#FFFFFF'},
  {color: '#0C7489', textColor: '#FFFFFF'},
  {color: '#D90368', textColor: '#FFFFFF'},
];
/**
 * It renders the about component
 */
class About extends BasisComponent {
  
  constructor(props) {
    super(props);
    //State
    this.state = {
      data: this.getTechnologies(),
    };
    //Listeners
    this._onTechnologyPressed = this.onTechnologyPressed.bind(this);
  }

  render() {

    return (
      <Box
        style={style().container}
        flex={false}
        direction={'row'}>
        
        <Box
          style={style().leftContainer}
          flex={true}
          justify={'center'}>
          {this.renderMissionAndVision(TXT_14, TXT_15)}
          {this.renderMissionAndVision(TXT_16, TXT_17)}
        </Box>
        
        <Box
          style={style(this.state.isSmartphone).technologiesContainer}
          flex={true}>
          {this.renderTitleWithAnimation(TXT_18)}
          {this.renderTechnologiesChart()}
        </Box>
      </Box>
    );
  }
  
  renderMissionAndVision(title, description) {
    
    return (
      <Animated
        animationIn={'fadeInUp'}
        animationOut={'fadeOutDown'}
        isVisible={this.props.animationVisibility}>
        
        <Box
          style={style().missionAndVisionContainer}
          align={'center'}>
          <Title>{title}</Title>
          <Paragraph align={'center'}>{description}</Paragraph>
        </Box>
      </Animated>
    );
  }
  
  renderTitleWithAnimation(title) {
    
    return (
      <Animated
        style={style().titleWithAnimationContainer}
        animationIn={'fadeInDown'}
        animationOut={'fadeOutUp'}
        isVisible={this.props.animationVisibility}>
        
        <Title
          style={style().titleWithAnimation}
          truncate={false}>
          {title}
        </Title>
      </Animated>
    );
  }
  
  renderTechnologiesChart() {
    
    return (
      this.props.animationVisibility &&
      <ReactBubbleChart
        colorLegend={colorLegend}
        legend={false}
        data={this.state.data}
        fixedDomain={{ min: 1, max: 20 }}
        fontSizeFactor={0.4}
        onClick={this._onTechnologyPressed}/>
    );
  }
  
  getTechnologies() {
    
    return TECHNOLOGIES.map((item, index) => ({
      _id: String(index),
      value: 1,
      url: item.url,
      displayText: String(item.name),
      colorValue: Math.floor((Math.random() * 20) + 1),
      selected: false,
    }));
  }
  
  onTechnologyPressed(data) {
    window.open(data.url);
  }
}

export default About;