export default function(isSmartphone) {
  
  return {
    
    container: {
      height: 'auto',
      minHeight: '93%',
      overflow: 'hidden',
    },
    leftContainer: {
      margin: '3% 3% 3% 3%',
      overflow: 'hidden',
    },
    missionAndVisionContainer: {
      backgroundColor: 'transparent',
    },
    technologiesContainer: {
      margin: '3% 3% 3% 0%',
      marginLeft: isSmartphone ? '3%' : '0%',
      marginTop: isSmartphone ? '0%' : '3%',
      overflow: 'hidden',
      height: isSmartphone && 400
    },
    titleWithAnimationContainer: {
      display: 'flex',
      justifyContent: 'center',
    },
    titleWithAnimation: {
      textAlign: 'center',
    },
  };
};