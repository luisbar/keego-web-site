//View ids
export const HOME = '/';
export const ABOUT = '/about';
export const TEAM = '/team';
export const PORTFOLIO = '/portfolio';
export const CONTACT = '/contact';

//Credentials
export const GOOGLE_API_KEY = 'AIzaSyD4FjkBi-L32Fi_SWi_2jXq9ypk-qy7Zv4';
export const RECAPTCHA_SITE_KEY = '6LcpwmQUAAAAAAAaqRr98nJBWBOIKfUQbBmePr3T';
export const BUGSNAG_KEY = 'ecabe95ea7f4773daac2ada7c03b02d1';

//Endpoints
export const GOOGLE_MAP_API = `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&v=3.exp&libraries=geometry,drawing,places`;
export const KEEGO_API = 'https://us-central1-luisbar-210717.cloudfunctions.net/';
export const SEND_EMAIL_URI = `${KEEGO_API}sendEmail`;

//Another stuffs
export const APP_VERSION = '0.5.1';
