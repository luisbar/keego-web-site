/**
 * Wrapper for logger tool
 */
export default class Logger {
  
  constructor(logger) {
    this.logger = logger;
  }
  
  notify(error) {
    this.logger.notify(JSON.stringify(error));
  }
}