/******************
 * Project import *
 ******************/
import {
  JSON_METHOD_ERROR,
  FETCH_API_ERROR,
} from '../../error';
/**
 * Wrapper in order to use the fetch API
 */
export default class Requester {
  /**
   * It makes a request with post to the endpoint specified
   * @param  {string} endpoint url
   * @param  {object} body data to send
   * @return {promise}
   */
  post(endpoint, body) {

    return new Promise((resolve, reject) => {
      fetch(endpoint, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify(body),
      })
      .then(this.errorHandler)
      .then((data) => resolve(data))
      .catch((error) => reject({
        errorType: error.errorType || 'client',
        errorCode: error.errorCode || FETCH_API_ERROR,
        errorMessage: error.errorMessage || 'Ocurrió un problema, lo sentimos :(',
        errorInfo: error.errorInfo || String(error),
      }));
    });
  }
  /**
   * It manages errors returned by our API
   * @param  {object} response response object
   */
  errorHandler(response) {

    return new Promise((resolve, reject) => {
      
      response.json()
      .then((data) => {

        switch (response.status) {

          case 200:
            if (!data.errorCode)
              resolve(data);
            else
              reject(data)
            break;

          default:
            reject(data);
        }
      })
      .catch((error) => reject({
        errorCode: JSON_METHOD_ERROR,
        errorInfo: error,
      }))
    })
  }
}
