/***********************
 * Node modules import *
 ***********************/
import bugsnag from 'bugsnag-js';
/******************
 * Project import *
 ******************/
import Requester from './requester';
import Logger from './logger';
import { BUGSNAG_KEY, APP_VERSION } from '../config';
/*************
 * Constants *
 *************/
const requester = new Requester();
const bugsnagClient = bugsnag({ apiKey: BUGSNAG_KEY, appVersion: APP_VERSION });
const logger = new Logger(bugsnagClient);

export {
  requester,
  logger,
};