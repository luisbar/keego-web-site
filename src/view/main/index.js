/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import App from 'grommet/components/App';
import Box from 'grommet/components/Box';
import { Route, Switch } from 'react-router-dom';
import { replace } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
/******************
 * Project import *
 ******************/
import style from './style';
import BasisComponent from '../basisComponent';
import animate  from '../../data/navigation/animate/action';
import executeSendEmail  from '../../data/email/sendEmail/action';
import executeFetchProductsByEnterprise  from '../../data/product/fetchProductsByEnterprise/action';
import { LeftSide, RightSide, Fss, ErrorBoundary } from './component/index';
import {
  TXT_1,
  TXT_10,
  TXT_11,
  TXT_12,
  TXT_13,
} from '../../string';
import {
  HOME,
  ABOUT,
  TEAM,
  PORTFOLIO,
  CONTACT,
} from '../../config';
import {
  Home,
  About,
  Team,
  Portfolio,
  Contact,
  NotFound,
  DesktopToolBar,
  MobileToolBar,
  MobileMenu
} from '../../component/index';
/**
 * Main component
 */
class Main extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Global variables
    this.fss = new Fss();
    //Listeners
    this._renderHome = this.renderHome.bind(this);
    this._renderAbout = this.renderAbout.bind(this);
    this._renderTeam = this.renderTeam.bind(this);
    this._renderPortfolio = this.renderPortfolio.bind(this);
    this._renderContact = this.renderContact.bind(this);
    this._renderNotFound = this.renderNotFound.bind(this);
    this._onMouseMove = this.onMouseMove.bind(this);
    this._onChangeMenuVisibility = this.onChangeMenuVisibility.bind(this);
    //State
    this.state = {
      menuVisibility: false,
    };
  }

  render() {

    return (
      <App
        centered={false}>
        <ErrorBoundary>
          <Box
            direction={'row'}
            full={true}>
            
            {!this.state.isSmartphone && 
              <LeftSide/>}
            {this.renderMiddleSide()}
            {!this.state.isSmartphone &&
              <RightSide
                replace={this.props.replace}
                location={this.props.location}
                animate={this.props.animate}
                animationVisibility={this.props.animationVisibility}/>
            }
            {
              this.state.isSmartphone &&
              <MobileMenu
                replace={this.props.replace}
                location={this.props.location}
                menuVisibility={this.state.menuVisibility}
                onCloseButtonPressed={this._onChangeMenuVisibility}/>
            }
          </Box>
        </ErrorBoundary>
      </App>
    );
  }
  
  renderMiddleSide() {
    
    return (
      <Box
        ref={'homeBackground'}
        full={true}
        onMouseMove={this._onMouseMove}
        style={style.background}>
        
        <Box
          id={'container'}
          style={this.props.location.pathname === HOME ? style.container : style.containerWithBackground }
          flex={true}>
          {this.renderContentHeader()}
          <Switch location={this.props.location}>
            <Route exact path={'/'} render={this._renderHome}/>
            <Route exact path={'/about'} render={this._renderAbout}/>
            <Route exact path={'/team'} render={this._renderTeam}/>
            <Route exact path={'/portfolio'} render={this._renderPortfolio}/>
            <Route exact path={'/contact'} render={this._renderContact}/>
            <Route render={this._renderNotFound}/>
          </Switch>
        </Box>
      </Box>
    );
  }
  
  renderContentHeader() {
    
    return (
      this.state.isSmartphone
      ? <MobileToolBar
          currentViewName={this.getCurrentViewName()}
          onHamburguerMenuButtonPressed={this._onChangeMenuVisibility}/>
      : <DesktopToolBar
          currentViewName={this.getCurrentViewName()}
          animationVisibility={this.props.animationVisibility}/>
    );
  }
  
  renderHome() {
    
    return (
      <Home
        animationVisibility={this.props.animationVisibility}/>
    );
  }
  
  renderAbout() {
    
    return (
      <About
        animationVisibility={this.props.animationVisibility}/>
    );
  }
  
  renderTeam() {
    
    return (
      <Team
        animationVisibility={this.props.animationVisibility}/>
    );
  }
  
  renderPortfolio() {
    
    return (
      <Portfolio
        productsByEnterprise={this.props.productsByEnterprise}
        fetchProductsByEnterpriseIsBeingExecuted={this.props.fetchProductsByEnterpriseIsBeingExecuted}
        statusOfFetchingProductsByEnterprise={this.props.statusOfFetchingProductsByEnterprise}
        executeFetchProductsByEnterprise={this.props.executeFetchProductsByEnterprise}
        animationVisibility={this.props.animationVisibility}/>
    );
  }
  
  renderContact() {
    
    return (
      <Contact
        animationVisibility={this.props.animationVisibility}
        executeSendEmail={this.props.executeSendEmail}
        statusOfSendingEmail={this.props.statusOfSendingEmail}
        sendEmailIsBeingExecuted={this.props.sendEmailIsBeingExecuted}/>
    );
  }
  
  renderNotFound() {
    
    return (
      <NotFound/>
    );
  }
  
  componentDidMount() {
    super.componentDidMount();
    this.fss.setParams(this.refs.homeBackground, 40, '#BEBEBE', '#424242', '#FFFFFF');
    this.fss.initialize();
    
    window.$('#container').niceScroll({
      cursorborder: '1px solid #424242',
      cursorcolor: '#B3CA21',
      cursorwidth: '5px',
    });
  }
  
  componentWillUnmount() {
    super.componentWillUnmount();
    this.fss.removeEventListeners();
  }
  
  onMouseMove(event) {
    this.fss.onMouseMove(event);
  }
  
  onChangeMenuVisibility(event) {
    this.setState({ menuVisibility: !this.state.menuVisibility });
  }
  
  getCurrentViewName() {
    
    switch (this.props.location.pathname) {
      
      case HOME:
        return TXT_1;
        
      case ABOUT:
        return TXT_10;
        
      case TEAM:
        return TXT_11;
        
      case PORTFOLIO:
        return TXT_12;
        
      case CONTACT:
        return TXT_13;
        
      default:
        return '';
    }
  }
}

const mapStateToProps = state => ({
  animationVisibility: state
    .rootReducer
    .dataReducer
    .navigationReducer
    .animateReducer.animationVisibility,
  statusOfSendingEmail: state
    .rootReducer
    .dataReducer
    .emailReducer
    .sendEmailReducer.statusOfSendingEmail,
  sendEmailIsBeingExecuted: state
    .rootReducer
    .dataReducer
    .emailReducer
    .sendEmailReducer.sendEmailIsBeingExecuted,
  productsByEnterprise: state
    .rootReducer
    .dataReducer
    .productReducer
    .fetchProductsByEnterpriseReducer.productsByEnterprise,
  fetchProductsByEnterpriseIsBeingExecuted: state
    .rootReducer
    .dataReducer
    .productReducer
    .fetchProductsByEnterpriseReducer.fetchProductsByEnterpriseIsBeingExecuted,
  statusOfFetchingProductsByEnterprise: state
    .rootReducer
    .dataReducer
    .productReducer
    .fetchProductsByEnterpriseReducer.statusOfFetchingProductsByEnterprise,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  replace: replace,
  animate: animate,
  executeSendEmail: executeSendEmail,
  executeFetchProductsByEnterprise: executeFetchProductsByEnterprise,
}, dispatch);

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Main));
