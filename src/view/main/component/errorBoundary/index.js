/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import Box from 'grommet/components/Box';
import Toast from 'grommet/components/Toast';
/******************
 * Project import *
 ******************/
import style from './style';
import { TXT_41 } from '../../../../string';
/**
 * It catchs all critical errors
 */
class ErrorBoundary extends PureComponent {
  
  constructor(props) {
    super(props);
    //State
    this.state = {
      error: null,
      errorInfo: null,
    };
    //Listeners
    this._onClosePressed = this.onClosePressed.bind(this);
  }

  render() {

    if (this.state.error)
      return (
        <Box>
          {this.props.children}
          <Toast
            status={'critical'}
            onClose={this._onClosePressed}>
            {TXT_41}
          </Toast>
        </Box>
      );
    else
      return this.props.children;
  }
  
  componentDidCatch(error, errorInfo) {
    this.setState({ error, errorInfo });
  }
  
  onClosePressed(event) {
    this.setState({ error: null, errorInfo: null });
  }
}

export default ErrorBoundary;