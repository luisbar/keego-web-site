export default {
  
  icon: {
    position: 'absolute',
    top: 15,
    right: 15,
    cursor: 'pointer',
  },
};