class Vertex {

  constructor(x, y, z, vectorThree) {
    this.vectorThree = vectorThree;
    this.position = this.vectorThree.create(x, y, z);
  }

  setPosition(x, y, z) {
    this.vectorThree.set(this.position, x, y, z);
    return this;
  }
}

export default Vertex;
