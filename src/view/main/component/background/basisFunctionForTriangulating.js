class BasisFunctionForTriangulating {

  constructor() {
    this.EPSILON = 1.0 / 1048576.0;
  }

  supertriangle(vertices) {
    let xmin = Number.POSITIVE_INFINITY;
    let ymin = Number.POSITIVE_INFINITY;
    let xmax = Number.NEGATIVE_INFINITY;
    let ymax = Number.NEGATIVE_INFINITY;
    let i;
    let dx;
    let dy;
    let dmax;
    let xmid;
    let ymid;

    for (i = vertices.length; i--;) {
      if (vertices[i][0] < xmin) xmin = vertices[i][0];
      if (vertices[i][0] > xmax) xmax = vertices[i][0];
      if (vertices[i][1] < ymin) ymin = vertices[i][1];
      if (vertices[i][1] > ymax) ymax = vertices[i][1];
    }

    dx = xmax - xmin;
    dy = ymax - ymin;
    dmax = Math.max(dx, dy);
    xmid = xmin + dx * 0.5;
    ymid = ymin + dy * 0.5;

    return [
      [xmid - 20 * dmax, ymid - dmax],
      [xmid, ymid + 20 * dmax],
      [xmid + 20 * dmax, ymid - dmax],
    ];
  }

  circumcircle(vertices, i, j, k) {
    let x1 = vertices[i][0];
    let y1 = vertices[i][1];
    let x2 = vertices[j][0];
    let y2 = vertices[j][1];
    let x3 = vertices[k][0];
    let y3 = vertices[k][1];
    let fabsy1y2 = Math.abs(y1 - y2);
    let fabsy2y3 = Math.abs(y2 - y3);
    let xc;
    let yc;
    let m1;
    let m2;
    let mx1;
    let mx2;
    let my1;
    let my2;
    let dx;
    let dy;

    /* Check for coincident points */
    if (fabsy1y2 < this.EPSILON && fabsy2y3 < this.EPSILON)
      throw new Error('Coincident points!');

    if (fabsy1y2 < this.EPSILON) {
      m2  = -((x3 - x2) / (y3 - y2));
      mx2 = (x2 + x3) / 2.0;
      my2 = (y2 + y3) / 2.0;
      xc  = (x2 + x1) / 2.0;
      yc  = m2 * (xc - mx2) + my2;
    } else if (fabsy2y3 < this.EPSILON) {
      m1  = -((x2 - x1) / (y2 - y1));
      mx1 = (x1 + x2) / 2.0;
      my1 = (y1 + y2) / 2.0;
      xc  = (x3 + x2) / 2.0;
      yc  = m1 * (xc - mx1) + my1;
    } else {
      m1  = -((x2 - x1) / (y2 - y1));
      m2  = -((x3 - x2) / (y3 - y2));
      mx1 = (x1 + x2) / 2.0;
      mx2 = (x2 + x3) / 2.0;
      my1 = (y1 + y2) / 2.0;
      my2 = (y2 + y3) / 2.0;
      xc  = (m1 * mx1 - m2 * mx2 + my2 - my1) / (m1 - m2);
      yc  = (fabsy1y2 > fabsy2y3) ?
        m1 * (xc - mx1) + my1 :
        m2 * (xc - mx2) + my2;
    }

    dx = x2 - xc;
    dy = y2 - yc;
    return { i: i, j: j, k: k, x: xc, y: yc, r: dx * dx + dy * dy };
  }

  dedup(edges) {
    let i;
    let j;
    let a;
    let b;
    let m;
    let n;

    for (j = edges.length; j;) {
      b = edges[--j];
      a = edges[--j];

      for (i = j; i;) {
        n = edges[--i];
        m = edges[--i];

        if ((a === m && b === n) || (a === n && b === m)) {
          edges.splice(j, 2);
          edges.splice(i, 2);
          break;
        }
      }
    }
  }
};

export default BasisFunctionForTriangulating;
