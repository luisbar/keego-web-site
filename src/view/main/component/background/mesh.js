/******************
 * Project import *
 ******************/
import CustomObject from './customObject.js';
import Geometry from './geometry.js';
import Material from './material.js';

class Mesh extends CustomObject {

  constructor(geometry, material, vectorThree, vectorFour, FSS) {
    super(vectorThree);
    this.geometry = geometry || new Geometry();
    this.material = material || new Material(null, null, vectorFour);
    this.side = FSS.FRONT;
    this.visible = true;
    this.FSS = FSS;
    this.vectorFour = vectorFour;
    this.vectorThree = vectorThree;
  }

  update(lights, calculate) {
    let t;
    let triangle;
    let l;
    let light;
    let illuminance;

    // Update Geometry
    this.geometry.update();

    // Calculate the triangle colors
    if (calculate) {

      // Iterate through Triangles
      for (t = this.geometry.triangles.length - 1; t >= 0; t--) {
        triangle = this.geometry.triangles[t];

        // Reset Triangle Color
        this.vectorFour.set(triangle.color.rgba);

        // Iterate through Lights
        for (l = lights.length - 1; l >= 0; l--) {
          light = lights[l];

          // Calculate Illuminance
          this.vectorThree.subtractVectors(light.ray, light.position, triangle.centroid);
          this.vectorThree.normalise(light.ray);
          illuminance = this.vectorThree.dot(triangle.normal, light.ray);
          if (this.side === this.FSS.FRONT) {
            illuminance = Math.max(illuminance, 0);
          } else if (this.side === this.FSS.BACK) {
            illuminance = Math.abs(Math.min(illuminance, 0));
          } else if (this.side === this.FSS.DOUBLE) {
            illuminance = Math.max(Math.abs(illuminance), 0);
          }

          // Calculate Ambient Light
          this.vectorFour.multiplyVectors(this.material.slave.rgba,
            this.material.ambient.rgba, light.ambient.rgba);
          this.vectorFour.add(triangle.color.rgba, this.material.slave.rgba);

          // Calculate Diffuse Light
          this.vectorFour.multiplyVectors(this.material.slave.rgba,
            this.material.diffuse.rgba, light.diffuse.rgba);
          this.vectorFour.multiplyScalar(this.material.slave.rgba, illuminance);
          this.vectorFour.add(triangle.color.rgba, this.material.slave.rgba);
        }

        // Clamp & Format Color
        this.vectorFour.clamp(triangle.color.rgba, 0, 0.7);
      }
    }

    return this;
  };
}

export default Mesh;
