/******************
 * Project import *
 ******************/
import CustomObject from './customObject.js';
import Color from './color.js';

class Light extends CustomObject {

  constructor(ambient, diffuse, vectorThree, vectorFour) {
    super(vectorThree);
    this.ambient = new Color(ambient || '#FFFFFF', null, vectorFour);
    this.diffuse = new Color(diffuse || '#FFFFFF', null, vectorFour);
    this.ray = vectorThree.create();
  }
}

export default Light;
