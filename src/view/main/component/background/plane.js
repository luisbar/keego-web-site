/******************
 * Project import *
 ******************/
import Vertex from './vertex.js';
import Geometry from './geometry.js';
import Triangle from './triangle.js';
import Triangulation from './triangulation.js';

class Plane extends Geometry {

  constructor(width, height, howmany, FSS, vectorThree, vectorFour) {
    super();
    this.FSS = FSS;
    this.vectorThree = vectorThree;
    this.vectorFour = vectorFour;

    this.width = width || 100;
    this.height = height || 100;

    // Cache Variables
    let x = new Array(howmany);
    let y = new Array(howmany);
    let vertices = new Array(howmany);
    let offsetX = this.width * -0.5;
    let offsetY = this.height * 0.5;

    for (var i = vertices.length; i--;) {
      x =  offsetX + Math.random() * width;
      y =  offsetY - Math.random() * height;

      vertices[i] = [x, y];
    }

    // Generate additional points on the perimeter so that there are no holes in the pattern
    vertices.push([offsetX, offsetY]);
    vertices.push([offsetX + width / 2, offsetY]);
    vertices.push([offsetX + width, offsetY]);
    vertices.push([offsetX + width, offsetY - height / 2]);
    vertices.push([offsetX + width, offsetY - height]);
    vertices.push([offsetX + width / 2, offsetY - height]);
    vertices.push([offsetX, offsetY - height]);
    vertices.push([offsetX, offsetY - height / 2]);

    // Generate additional randomly placed points on the perimeter
    for (let i = 6; i >= 0; i--) {
      vertices.push([offsetX + Math.random() * width, offsetY]);
      vertices.push([offsetX, offsetY - Math.random() * height]);
      vertices.push([offsetX + width, offsetY - Math.random() * height]);
      vertices.push([offsetX + Math.random() * width, offsetY - height]);
    }

    // Create an array of triangulated coordinates from our vertices
    let triangles = new Triangulation().triangulate(vertices);

    for (i = triangles.length; i;) {
      --i;
      let v1 = new Vertex(
        Math.ceil(vertices[triangles[i]][0]),
        Math.ceil(vertices[triangles[i]][1]),
        null,
        this.vectorThree
      );
      --i;
      let v2 = new Vertex(
        Math.ceil(vertices[triangles[i]][0]),
        Math.ceil(vertices[triangles[i]][1]),
        null,
        this.vectorThree
      );
      --i;
      let v3 = new Vertex(
        Math.ceil(vertices[triangles[i]][0]),
        Math.ceil(vertices[triangles[i]][1]),
        null,
        this.vectorThree);
      let t1 = new Triangle(v1, v2, v3, this.vectorThree, this.vectorFour, this.FSS);
      this.triangles.push(t1);
      this.vertices.push(v1);
      this.vertices.push(v2);
      this.vertices.push(v3);
    }
  };
}

export default Plane;
