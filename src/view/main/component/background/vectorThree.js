class VectorThree {

  constructor(FSS) {
    this.FSS = FSS;
  }

  create(x, y, z) {
    let vector = new Array(3);
    this.set(vector, x, y, z);
    return vector;
  }

  clone(a) {
    let vector = this.create();
    this.copy(vector, a);
    return vector;
  }

  set(target, x, y, z) {
    target[0] = x || 0;
    target[1] = y || 0;
    target[2] = z || 0;
    return this;
  }

  setX(target, x) {
    target[0] = x || 0;
    return this;
  }

  setY(target, y) {
    target[1] = y || 0;
    return this;
  }

  setZ(target, z) {
    target[2] = z || 0;
    return this;
  }

  copy(target, a) {
    target[0] = a[0];
    target[1] = a[1];
    target[2] = a[2];
    return this;
  }

  add(target, a) {
    target[0] += a[0];
    target[1] += a[1];
    target[2] += a[2];
    return this;
  }

  addVectors(target, a, b) {
    target[0] = a[0] + b[0];
    target[1] = a[1] + b[1];
    target[2] = a[2] + b[2];
    return this;
  }

  addScalar(target, s) {
    target[0] += s;
    target[1] += s;
    target[2] += s;
    return this;
  }

  subtract(target, a) {
    target[0] -= a[0];
    target[1] -= a[1];
    target[2] -= a[2];
    return this;
  }

  subtractVectors(target, a, b) {
    target[0] = a[0] - b[0];
    target[1] = a[1] - b[1];
    target[2] = a[2] - b[2];
    return this;
  }

  subtractScalar(target, s) {
    target[0] -= s;
    target[1] -= s;
    target[2] -= s;
    return this;
  }

  multiply(target, a) {
    target[0] *= a[0];
    target[1] *= a[1];
    target[2] *= a[2];
    return this;
  }

  multiplyVectors(target, a, b) {
    target[0] = a[0] * b[0];
    target[1] = a[1] * b[1];
    target[2] = a[2] * b[2];
    return this;
  }

  multiplyScalar(target, s) {
    target[0] *= s;
    target[1] *= s;
    target[2] *= s;
    return this;
  }

  divide(target, a) {
    target[0] /= a[0];
    target[1] /= a[1];
    target[2] /= a[2];
    return this;
  }

  divideVectors(target, a, b) {
    target[0] = a[0] / b[0];
    target[1] = a[1] / b[1];
    target[2] = a[2] / b[2];
    return this;
  }

  divideScalar(target, s) {
    if (s !== 0) {
      target[0] /= s;
      target[1] /= s;
      target[2] /= s;
    } else {
      target[0] = 0;
      target[1] = 0;
      target[2] = 0;
    }

    return this;
  }

  cross(target, a) {
    let x = target[0];
    let y = target[1];
    let z = target[2];
    target[0] = y * a[2] - z * a[1];
    target[1] = z * a[0] - x * a[2];
    target[2] = x * a[1] - y * a[0];
    return this;
  }

  crossVectors(target, a, b) {
    target[0] = a[1] * b[2] - a[2] * b[1];
    target[1] = a[2] * b[0] - a[0] * b[2];
    target[2] = a[0] * b[1] - a[1] * b[0];
    return this;
  }

  min(target, value) {
    if (target[0] < value) target[0] = value;
    if (target[1] < value) target[1] = value;
    if (target[2] < value) target[2] = value;
    return this;
  }

  max(target, value) {
    if (target[0] > value) target[0] = value;
    if (target[1] > value) target[1] = value;
    if (target[2] > value) target[2] = value;
    return this;
  }

  clamp(target, min, max) {
    this.min(target, min);
    this.max(target, max);
    return this;
  }

  limit(target, min, max) {
    let length = this.length(target);
    if (min !== null && length < min) {
      this.setLength(target, min);
    } else if (max !== null && length > max) {
      this.setLength(target, max);
    }

    return this;
  }

  dot(a, b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
  }

  normalise(target) {
    return this.divideScalar(target, this.length(target));
  }

  negate(target) {
    return this.multiplyScalar(target, -1);
  }

  distanceSquared(a, b) {
    let dx = a[0] - b[0];
    let dy = a[1] - b[1];
    let dz = a[2] - b[2];
    return dx * dx + dy * dy + dz * dz;
  }

  distance(a, b) {
    return Math.sqrt(this.distanceSquared(a, b));
  }

  lengthSquared(a) {
    return a[0] * a[0] + a[1] * a[1] + a[2] * a[2];
  }

  length(a) {
    return Math.sqrt(this.lengthSquared(a));
  }

  setLength(target, l) {
    let length = this.length(target);
    if (length !== 0 && l !== length) {
      this.multiplyScalar(target, l / length);
    }

    return this;
  }
}

export default VectorThree;
