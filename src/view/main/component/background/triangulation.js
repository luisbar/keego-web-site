import BasisFunctionForTriangulating from './basisFunctionForTriangulating.js';

class Triangulation {

  constructor() {
    this.basisFunctionForTriangulating = new BasisFunctionForTriangulating();
    this.EPSILON = 1.0 / 1048576.0;
  }

  triangulate(vertices, key) {
    let n = vertices.length;
    let i;
    let j;
    let indices;
    let st;
    let open;
    let closed;
    let edges;
    let dx;
    let dy;
    let a;
    let b;
    let c;

    /**
     * Bail if there aren't enough vertices to form any triangles.
     */
    if (n < 3)
      return [];

    /*
     * Slice out the actual vertices from the passed objects. (Duplicate the
     * array even if we don't, though, since we need to make a supertriangle
     * later on!)
    */
    vertices = vertices.slice(0);

    if (key)
      for (i = n; i--;)
        vertices[i] = vertices[i][key];

    /* Make an array of indices into the vertex array, sorted by the
     * vertices' x-position. */
    indices = new Array(n);

    for (i = n; i--;)
      indices[i] = i;

    indices.sort(function (i, j) {
      return vertices[j][0] - vertices[i][0];
    });

    /* Next, find the vertices of the supertriangle (which contains all other
     * triangles), and append them onto the end of a (copy of) the vertex
     * array. */
    st = this.basisFunctionForTriangulating.supertriangle(vertices);
    vertices.push(st[0], st[1], st[2]);

    /* Initialize the open list (containing the supertriangle and nothing
     * else) and the closed list (which is empty since we havn't processed
     * any triangles yet). */
    open   = [this.basisFunctionForTriangulating.circumcircle(vertices, n + 0, n + 1, n + 2)];
    closed = [];
    edges  = [];

    /* Incrementally add each vertex to the mesh. */
    for (i = indices.length; i--; edges.length = 0) {
      c = indices[i];

      /* For each open triangle, check to see if the current point is
       * inside it's circumcircle. If it is, remove the triangle and add
       * it's edges to an edge list. */
      for (j = open.length; j--;) {
        /* If this point is to the right of this triangle's circumcircle,
         * then this triangle should never get checked again. Remove it
         * from the open list, add it to the closed list, and skip. */
        dx = vertices[c][0] - open[j].x;
        if (dx > 0.0 && dx * dx > open[j].r) {
          closed.push(open[j]);
          open.splice(j, 1);
          continue;
        }

        /* If we're outside the circumcircle, skip this triangle. */
        dy = vertices[c][1] - open[j].y;
        if (dx * dx + dy * dy - open[j].r > this.EPSILON)
          continue;

        /* Remove the triangle and add it's edges to the edge list. */
        edges.push(
          open[j].i, open[j].j,
          open[j].j, open[j].k,
          open[j].k, open[j].i
        );
        open.splice(j, 1);
      }

      /* Remove any doubled edges. */
      this.basisFunctionForTriangulating.dedup(edges);

      /* Add a new triangle for each edge. */
      for (j = edges.length; j;) {
        b = edges[--j];
        a = edges[--j];
        open.push(this.basisFunctionForTriangulating.circumcircle(vertices, a, b, c));
      }
    }

    /* Copy any remaining open triangles to the closed list, and then
     * remove any triangles that share a vertex with the supertriangle,
     * building a list of triplets that represent triangles. */
    for (i = open.length; i--;)
      closed.push(open[i]);
    open.length = 0;

    for (i = closed.length; i--;)
      if (closed[i].i < n && closed[i].j < n && closed[i].k < n)
        open.push(closed[i].i, closed[i].j, closed[i].k);

    /* Yay, we're done! */
    return open;
  }
};

export default Triangulation;
