class Color {

  constructor(hex, opacity, vectorFour) {
    this.rgba = vectorFour.create();
    this.hex = hex || '#000000';
    this.opacity = this.isNumber(opacity) ? opacity : 1;
    this.set(this.hex, this.opacity);
  }

  set(hex, opacity) {
    hex = hex.replace('#', '');
    var size = hex.length / 3;
    this.rgba[0] = parseInt(hex.substring(size * 0, size * 1), 16) / 255;
    this.rgba[1] = parseInt(hex.substring(size * 1, size * 2), 16) / 255;
    this.rgba[2] = parseInt(hex.substring(size * 2, size * 3), 16) / 255;
    this.rgba[3] = this.isNumber(opacity) ? opacity : this.rgba[3];
    return this;
  }

  hexify(channel) {
    var hex = Math.ceil(channel * 255).toString(16);
    if (hex.length === 1) hex = '0' + hex;
    return hex;
  }

  format() {
    var r = this.hexify(this.rgba[0]);
    var g = this.hexify(this.rgba[1]);
    var b = this.hexify(this.rgba[2]);
    this.hex = '#' + r + g + b;
    return this.hex;
  }

  isNumber(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
  }
}

export default Color;
