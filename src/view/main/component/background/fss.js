//============================================================
//
// Copyright (C) 2013 Matthew Wagerfield
//
// Twitter: https://twitter.com/mwagerfield
//
// Permission is hereby granted, free of charge, to any
// person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the
// Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice
// shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY
// OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
// EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
// AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
// OR OTHER DEALINGS IN THE SOFTWARE.
//
//============================================================
import VectorThree from './vectorThree.js';
import VectorFour from './vectorFour.js';

import CanvasRenderer from './canvasRenderer.js';
import Scene from './scene.js';
import Mesh from './mesh.js';
import Material from './material.js';
import Plane from './plane.js';
import Light from './light.js';

class FSS {

  constructor() {
    this.FRONT = 0;
    this.BACK = 1;
    this.DOUBLE = 2;
    this.SVGNS = 'http://www.w3.org/2000/svg';

    this.vectorThree = new VectorThree(this);
    this.vectorFour = new VectorFour(this);

    this.renderer;
    this.light;
    this.scene;
    this.mesh;
    this.material;
    this.plane;
    this.center = this.vectorThree.create();

    //Customizables parameters
    this.container;
    this.triangleQuantities;
    this.mainColor;
    this.secondaryColor;
    this.lightColor;
  }

  setParams(
    container,
    triangleQuantities,
    mainColor,
    secondaryColor,
    lightColor) {
    this.container = container;
    this.triangleQuantities = triangleQuantities;
    this.mainColor = mainColor;
    this.secondaryColor = secondaryColor;
    this.lightColor = lightColor;
  }

  initialize() {
    this.createRenderer();
    this.createScene();
    this.createMesh();
    this.addLight();
    this.resize(this.container.boxContainerRef.offsetWidth,
    this.container.boxContainerRef.offsetHeight);
    this.animate();
    this.addEventListeners();
  }

  createRenderer() {
    this.renderer = new CanvasRenderer();
    this.renderer.setSize(this.container.boxContainerRef.offsetWidth,
    this.container.boxContainerRef.offsetHeight);
    this.container.boxContainerRef.appendChild(this.renderer.element);
  }

  createScene() {
    this.scene = new Scene(this);
  }

  createMesh() {
    this.scene.remove(this.mesh);
    this.renderer.clear();
    this.geometry = new Plane(this.container.boxContainerRef.offsetWidth,
      this.container.boxContainerRef.offsetHeight,
      this.triangleQuantities, this, this.vectorThree, this.vectorFour);
    this.material = new Material(this.lightColor, this.lightColor, this.vectorFour, this);
    this.mesh = new Mesh(this.geometry, this.material, this.vectorThree, this.vectorFour, this);
    this.scene.add(this.mesh);
  }

  addLight() {
    this.renderer.clear();
    this.light = new Light(this.secondaryColor,
      this.mainColor, this.vectorThree, this.vectorFour, this);
    this.light.ambientHex = this.light.ambient.format();
    this.light.diffuseHex = this.light.diffuse.format();
    this.light.setPosition(0, 0, 100);
    this.scene.add(this.light);
  }

  resize(width, height) {
    this.renderer.setSize(width, height);
    this.vectorThree.set(this.center, this.renderer.halfWidth, this.renderer.halfHeight);
    this.createMesh(this.container);
  }

  animate() {
    this.renderBackgorund();
  }

  renderBackgorund() {
    this.renderer.render(this.scene);
  }

  addEventListeners() {
    window.addEventListener('resize', (event) => this.onWindowResize(event));
  }

  removeEventListeners() {
    window.removeEventListener('resize', (event) => this.onWindowResize(event));
  }

  onMouseMove(event) {
    let x = event.screenX - this.renderer.width / 2;
    let y = (this.renderer.height / 2) - (event.screenY);

    this.light.setPosition(x, y, 100);
    this.renderer.render(this.scene);
  }

  onWindowResize(event) {
    if (this.container && this.container.boxContainerRef) {
      this.resize(this.container.boxContainerRef.offsetWidth,
      this.container.boxContainerRef.offsetHeight, this.container);
      this.renderBackgorund();
    }
  }
}

export default FSS;
