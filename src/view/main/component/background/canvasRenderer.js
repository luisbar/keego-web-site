/******************
 * Project import *
 ******************/
import Renderer from './renderer.js';

class CanvasRenderer extends Renderer {

  constructor() {
    super();
    this.element = document.createElement('canvas');
    this.element.style.display = 'block';
    this.context = this.element.getContext('2d');
    this.setSize(this.element.width, this.element.height);
  }

  setSize(width, height) {
    super.setSize(width, height);
    this.element.width = width;
    this.element.height = height;
    this.context.setTransform(1, 0, 0, -1, this.halfWidth, this.halfHeight);
    return this;
  }

  clear() {
    super.clear();
    this.context.clearRect(-this.halfWidth, -this.halfHeight, this.width, this.height);
    return this;
  }

  render(scene) {
    super.render(scene);
    let m;
    let mesh;
    let t;
    let triangle;
    let color;

    // Clear Context
    this.clear();

    // Configure Context
    this.context.lineJoin = 'round';
    this.context.lineWidth = 1;

    // Update Meshes
    for (m = scene.meshes.length - 1; m >= 0; m--) {
      mesh = scene.meshes[m];
      if (mesh.visible) {
        mesh.update(scene.lights, true);

        // Render Triangles
        for (t = mesh.geometry.triangles.length - 1; t >= 0; t--) {
          triangle = mesh.geometry.triangles[t];
          color = triangle.color.format();
          this.context.beginPath();
          this.context.moveTo(triangle.a.position[0], triangle.a.position[1]);
          this.context.lineTo(triangle.b.position[0], triangle.b.position[1]);
          this.context.lineTo(triangle.c.position[0], triangle.c.position[1]);
          this.context.closePath();
          this.context.strokeStyle = color;
          this.context.fillStyle = color;
          this.context.stroke();
          this.context.fill();
          // this.context.save();
          //
          // this.context.scale(1, -1);
          //
          // // Text Added at the origin of the newly transformed canvas
          // this.context.font = '24px Arial';
          // this.context.fillStyle = '#474747';
          // this.context.fillText('Sample', triangle.a.position[0], triangle.a.position[1]);
          //
          // // Context restored back
          // this.context.restore();
        }
      }
    }

    return this;
  }
}

export default CanvasRenderer;
