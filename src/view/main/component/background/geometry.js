class Geometry {

  constructor() {
    this.vertices = [];
    this.triangles = [];
    this.dirty = false;
  }

  update() {
    if (this.dirty) {
      let t;
      let triangle;

      for (t = this.triangles.length - 1; t >= 0; t--) {
        triangle = this.triangles[t];
        triangle.computeCentroid();
        triangle.computeNormal();
      }

      this.dirty = false;
    }

    return this;
  }
}

export default Geometry;
