/******************
 * Project import *
 ******************/
import Color from './color.js';

class Material {

  constructor(ambient, diffuse, vectorFour) {
    this.ambient = new Color(ambient || '#444444', null, vectorFour);
    this.diffuse = new Color(diffuse || '#FFFFFF', null, vectorFour);
    this.slave = new Color(null, null, vectorFour);
  }
}

export default Material;
