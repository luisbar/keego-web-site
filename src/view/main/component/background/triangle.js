/******************
 * Project import *
 ******************/
import Vertex from './vertex.js';
import Color from './color.js';

class Triangle {

  constructor(a, b, c, vectorThree, vectorFour, FSS) {
    this.vectorThree = vectorThree;
    this.a = a || new Vertex(null, null, null, this.vectorThree);
    this.b = b || new Vertex(null, null, null, this.vectorThree);
    this.c = c || new Vertex(null, null, null, this.vectorThree);
    this.vertices = [this.a, this.b, this.c];
    this.u = this.vectorThree.create();
    this.v = this.vectorThree.create();
    this.centroid = this.vectorThree.create();
    this.normal = this.vectorThree.create();
    this.color = new Color(null, null, vectorFour);
    this.polygon = document.createElementNS(FSS.SVGNS, 'polygon');
    this.polygon.setAttributeNS(null, 'stroke-linejoin', 'round');
    this.polygon.setAttributeNS(null, 'stroke-miterlimit', '1');
    this.polygon.setAttributeNS(null, 'stroke-width', '1');
    this.computeCentroid();
    this.computeNormal();
  }

  computeCentroid() {
    this.centroid[0] = this.a.position[0] + this.b.position[0] + this.c.position[0];
    this.centroid[1] = this.a.position[1] + this.b.position[1] + this.c.position[1];
    this.centroid[2] = this.a.position[2] + this.b.position[2] + this.c.position[2];
    this.vectorThree.divideScalar(this.centroid, 3);
    return this;
  }

  computeNormal() {
    this.vectorThree.subtractVectors(this.u, this.b.position, this.a.position);
    this.vectorThree.subtractVectors(this.v, this.c.position, this.a.position);
    this.vectorThree.crossVectors(this.normal, this.u, this.v);
    this.vectorThree.normalise(this.normal);
    return this;
  }
}

export default Triangle;
