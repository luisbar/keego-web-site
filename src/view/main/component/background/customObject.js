class CustomObject {

  constructor(vectorThree) {
    this.vectorThree = vectorThree;
    this.position = this.vectorThree.create();
  };

  setPosition(x, y, z) {
    this.vectorThree.set(this.position, x, y, z);
    return this;
  }
}

export default CustomObject;
