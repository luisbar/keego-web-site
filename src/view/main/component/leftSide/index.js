/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Sidebar from 'grommet/components/Sidebar';
import Header from 'grommet/components/Header';
import Image from 'grommet/components/Image';
import Footer from 'grommet/components/Footer';
import ReactTooltip from 'react-tooltip';
/******************
 * Project import *
 ******************/
import './style.css';
import style from './style';
import BasisComponent from '../../../basisComponent.js'
import { FacebookIcon, TwitterIcon } from '../../../../component/index';
import { FACEBOOK, TWITTER } from '../../../../string';
/**
 * It renders the left side of the web site
 */
class LeftSide extends BasisComponent {
  
  render() {

    return (
      <Sidebar
        colorIndex={'neutral-1'}
        size={'xsmall'}
        justify={'between'}>
        
        <Header
          style={style.header}
          direction={'column'}
          align={'center'}
          onClick={this._onLogoPressed}
          data-tip={'Nuestro nombre es un acronimo de dos palabras en Ingles, keep y going, que significa seguir adelante'}>
          {this.renderLogo()}
          {this.renderTitle()}
        </Header>
        
        <ReactTooltip
          className={'tooltip'}
          event={'click'}
          effect={'solid'}
          place={'bottom'}
          offset={{ top: -50 }}/>
        
        <Footer
          justify={'center'}
          wrap={true}>
          {this.renderFacebookIcon()}
          {this.renderTwitterIcon()}
        </Footer>
      </Sidebar>
    );
  }
  
  renderLogo() {
    
    return (
      <Image
        style={style.logo}
        src={'./resource/image/logo.svg'}
        size={'small'}/>
    );
  }
  
  renderTitle() {
    
    return (
      <Image
        style={style.title}
        src={'./resource/image/enterprise-name.svg'}
        size={'small'}/>
    );
  }
  
  renderFacebookIcon() {
    
    return (
      <FacebookIcon
        primaryIconColor={'#B3CA21'}
        secondaryIconColor={'#666666'}
        url={FACEBOOK}/>
    );
  }
  
  renderTwitterIcon() {
    
    return (
      <TwitterIcon
        primaryIconColor={'#B3CA21'}
        secondaryIconColor={'#666666'}
        url={TWITTER}/>
    );
  }
}

export default LeftSide;