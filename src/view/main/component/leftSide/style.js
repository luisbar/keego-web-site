export default {
  
  header: {
    cursor: 'pointer',
    boxShadow: '0 0 0 0',
  },
  logo: {
    padding: 5,
    margin: 0,
  },
  title: {
    padding: 10,
  },
};