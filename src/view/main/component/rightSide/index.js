/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Sidebar from 'grommet/components/Sidebar';
import Box from 'grommet/components/Box';
import Heading from 'grommet/components/Heading';
import Home from 'grommet/components/icons/base/Home';
import Information from 'grommet/components/icons/base/CircleInformation';
import Group from 'grommet/components/icons/base/Group';
import Briefcase from 'grommet/components/icons/base/Briefcase';
import Contact from 'grommet/components/icons/base/AccessVolumeControl';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import './style.css';
import style from './style';
import BasisComponent from '../../../basisComponent.js'
import {
  TXT_5,
  TXT_6,
  TXT_7,
  TXT_8,
  TXT_9,
} from '../../../../string.js';
import {
  HOME,
  ABOUT,
  TEAM,
  PORTFOLIO,
  CONTACT,
} from '../../../../config.js';
/**
 * It renders the right side of the web site
 */
class RightSide extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Global variables
    this.viewIdPressed = props.location.pathname;
    //Listeners
    this._onMouseEnter = (viewId) => this.onMouseEnter.bind(this, viewId);
    this._onMouseLeave = (viewId) => this.onMouseLeave.bind(this, viewId);
    this._onMenuItemPressed = (viewId) => this.onMenuItemPressed.bind(this, viewId);
    //State
    this.state = {
      [HOME]: false,
      [ABOUT]: false,
      [TEAM]: false,
      [PORTFOLIO]: false,
      [CONTACT]: false,
    };
  }
  
  componentWillReceiveProps(nextProps) {
    if (!this.props.animationVisibility &&
      nextProps.animationVisibility)
      this.props.replace(this.viewIdPressed);
  }

  render() {

    return (
      <Sidebar
        style={style.sidebar}
        colorIndex={'neutral-1'}
        size={'xsmall'}>
        {this.renderMenuItem(HOME, TXT_5)}
        {this.renderMenuItem(ABOUT, TXT_6)}
        {this.renderMenuItem(TEAM, TXT_7)}
        {this.renderMenuItem(PORTFOLIO, TXT_8)}
        {this.renderMenuItem(CONTACT, TXT_9)}
      </Sidebar>
    );
  }

  renderMenuItem(viewId, text) {

    return (
      <Box
        className={this.viewIdPressed === viewId ? 'sweepInFromLeft' : 'sweepOutToLeft'}
        style={this.getStyleForMenuItem(viewId)}
        flex={true}
        onMouseEnter={this._onMouseEnter(viewId)}
        onMouseLeave={this._onMouseLeave(viewId)}
        onClick={this._onMenuItemPressed(viewId)}>
        
        <Box
          direction={'row'}
          justify={'center'}
          align={'center'}
          flex={true}>
          {this.renderItemPosition(viewId)}
          {this.renderItemIcon(viewId)}
        </Box>
        
        <Box
          flex={true}
          justify={'center'}>
          {this.renderItemText(viewId, text)}
        </Box>
      </Box>
    );
  }
  
  renderItemPosition(viewId) {
    
    return (
      <Heading
        style={style.number}
        tag={'h6'}>
        {this.getPosition(viewId)}
      </Heading>
    );
  }
  
  renderItemIcon(viewId) {
    
    switch (viewId) {
      
      case HOME:
        return <Home
                size={'small'}/>
      case ABOUT:
        return <Information
                size={'small'}/>
      case TEAM:
        return <Group
                size={'small'}/>
      case PORTFOLIO:
        return <Briefcase
                size={'small'}/>
      case CONTACT:
        return <Contact
                size={'small'}/>
    };
  }
  
  renderItemText(viewId, text) {
    
    return (
      <Heading
        style={this.getStyleForItemText(viewId)}
        tag={'h6'}>
        {text}
      </Heading>
    );
  }
  
  onMouseEnter(viewId, event) {
    this.setState({
      [viewId]: true,
    });
  }
  
  onMouseLeave(viewId, event) {
    this.setState({
      [viewId]: false,
    });
  }
  
  onMenuItemPressed(viewId, event) {
    if (viewId !== this.props.location.pathname) {
      this.viewIdPressed = viewId;
      this.props.animate();
    }
  }
  
  getStyleForMenuItem(viewId) {
    
    return Object.assign(
      {},
      style.menuItem,
      this.state[viewId] && style.menuItemHover
    );
  }
  
  getPosition(viewId) {
    
    switch (viewId) {

      case HOME:
        return '01';
      case ABOUT:
        return '02';
      case TEAM:
        return '03';
      case PORTFOLIO:
        return '04';
      case CONTACT:
        return '05';
    }
  }
  
  getStyleForItemText(viewId) {
    
    return Object.assign(
      {},
      style.text,
      !this.state[viewId] && style.textLeft
    );
  } 
}

RightSide.propTypes = {
  replace: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  animate: PropTypes.func.isRequired,
  animationVisibility: PropTypes.bool.isRequired,
};

export default RightSide;