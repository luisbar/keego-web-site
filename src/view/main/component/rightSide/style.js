export default {

  sidebar: {
    overflow: 'visible',
  },
  menuItem: {
    cursor: 'pointer',
    backgroundColor: '#424242',
    borderStyle: 'solid',
    borderColor: '#B2CA21',
    borderLeftWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    boxShadow: '0 0 0 0',
    zIndex: 2,
  },
  menuItemHover: {
    width: '150%',
    right: '50%',
    position: 'relative',
    borderColor: '#B3CA21',
    boxShadow: '-5px 5px 20px 0 #424242',
  },
  number: {
    color: '#FFFFFF',
    margin: 0,
    display: 'flex',
    alignItems: 'center',
    marginRight: 5,
  },
  text: {
    color: '#FFFFFF',
    margin: 0,
    width: '100%',
    textAlign: 'center',
  },
  textLeft: {
    opacity: 0,
  },
};
