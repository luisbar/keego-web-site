export default {
  
  background: {
    position: 'relative',
    backgroundColor: '#666666',
  },
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  containerWithBackground: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#666666'
  },
};