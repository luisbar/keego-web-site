/***********************
 * Node modules import *
 ***********************/
import { combineReducers } from 'redux';
/******************
 * Project import *
 ******************/
import sendEmailReducer from './sendEmail/reducer';

export default combineReducers({
  sendEmailReducer,
});