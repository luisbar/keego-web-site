/******************
 * Project import *
 ******************/
import {
  WILL_SEND_EMAIL,
  SEND_EMAIL,
  DID_SEND_EMAIL,
} from './actionType';
/*************
 * Constants *
 *************/
const initialState = {
  sendEmailIsBeingExecuted: false,
  statusOfSendingEmail: {
    wasError: false,
    error: null,
  },
};
/**
 * It manages all changes on the store due to
 * send email action
 */
export default function sendEmail(state = initialState, action) {
  
  switch (action.type) {

    case WILL_SEND_EMAIL:
      return Object.assign({}, state, {
        sendEmailIsBeingExecuted: true,
        statusOfSendingEmail: {
          wasError: false,
          error: null,
        },
      });
    
    case SEND_EMAIL:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSendingEmail: {
            wasError: true,
            error: action.payload,
          },
        });
      else
        return state;
      
    case DID_SEND_EMAIL:
      return Object.assign({}, state, {
        sendEmailIsBeingExecuted: false,
      });
      
    default:
      return state;
  }
};