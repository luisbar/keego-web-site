/******************
 * Project import *
 ******************/
import { SEND_EMAIL_URI } from '../../../config';
import { requester, logger } from '../../../service/index';
import {
  WILL_SEND_EMAIL,
  SEND_EMAIL,
  DID_SEND_EMAIL,
} from './actionType';
/**
 * It executes the process for sending an email
 * @param  {string} name name of user
 * @param  {string} email email of the user
 * @param  {string} subject subject of the message
 * @param  {string} message message
 * @param  {string} recaptchaToken token for recaptcha service
 */
export default function executeSendEmail(name, email, subject, message, recaptchaToken) {
  
  return (dispatch) => {
    dispatch(willSendMail());
    
    requester.post(
      SEND_EMAIL_URI,
      {
        name,
        email,
        subject,
        message,
        recaptchaToken
      }
    )
    .then((response) => {
      dispatch(sendEmail(false, response));
      dispatch(didSendEmail());
    })
    .catch((error) => {
      logger.notify(error);
      dispatch(sendEmail(true, error));
      dispatch(didSendEmail());
    })
  };
}
/**
 * It changes the state in order to know that the
 * process for sending an email will be executed
 */
function willSendMail() {
  
  return {
    type: WILL_SEND_EMAIL,
  };
}
/**
 * It changes the state in order to know that the
 * process for sending an email has been executed recently
 */
function sendEmail(error, payload) {
  
  return {
    type: SEND_EMAIL,
    payload: payload,
    error: error,
  };
}
/**
 * It changes the state in order to know that the
 * process for sending an email has been executed completely
 */
function didSendEmail() {
  
  return {
    type: DID_SEND_EMAIL,
  };
}