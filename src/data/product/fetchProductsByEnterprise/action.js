/******************
* Project import *
******************/
import {
  WILL_FETCH_PRODUCTS_BY_ENTERPRISE,
  FETCH_PRODUCTS_BY_ENTERPRISE,
  DID_FETCH_PRODUCTS_BY_ENTERPRISE,
} from './actionType';

import db from '../../../index';
/**
 * It executes the process for fetching products
 * by enterprise
 */
export default function executeFetchProductsByEnterprise() {
  let data = [];
  
  return (dispatch) => {
    dispatch(willFetchProductsByEnterprise());
    
    db.collection('enterprise')
    .get()
    .then((enterprisesSnapshot) => {
      let promises = [];

      enterprisesSnapshot.forEach((enterpriseDocument) => {
        data.push(enterpriseDocument.data());
        promises.push(db.collection('enterprise')
        .doc(enterpriseDocument.id)
        .collection('products')
        .get());
      });
      
      return Promise.all(promises);
    })
    .then((productSnapshots) => {
      var i = 0;
      let enterprises = [];      
      
      productSnapshots.map((productSnapshot) => {
        let products = [];
        productSnapshot.forEach((productDocument) => {
          
          products.push(productDocument.data());
        })
        
        enterprises.push(Object.assign({}, data[i], {
            products: products,
          })
        );
        i++;
      })
      
      return enterprises;
    })
    .then((enterprises) => {
      dispatch(fetchProductsByEnterprise(false, enterprises));
      dispatch(didFetchProductsByEnterprise());
    })
    .catch((error) => {
      dispatch(fetchProductsByEnterprise(true, error));
      dispatch(didFetchProductsByEnterprise());
    });
  };
}
/**
 * It changes the state in order to know that the
 * process for fetching products by enterprise will be executed
 */
function willFetchProductsByEnterprise() {
  
  return {
    type: WILL_FETCH_PRODUCTS_BY_ENTERPRISE,
  };
}
/**
 * It changes the state in order to know that the
 * process for fetching products by enterprise has
 * been executed recently
 */
function fetchProductsByEnterprise(error, payload) {
  
  return {
    type: FETCH_PRODUCTS_BY_ENTERPRISE,
    payload: payload,
    error: error,
  };
}
/**
 * It changes the state in order to know that the
 * process for fetching products by enterprise has
 * been executed completely
 */
function didFetchProductsByEnterprise() {
  
  return {
    type: DID_FETCH_PRODUCTS_BY_ENTERPRISE,
  };
}