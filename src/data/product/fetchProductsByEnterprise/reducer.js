/******************
 * Project import *
 ******************/
 import {
   WILL_FETCH_PRODUCTS_BY_ENTERPRISE,
   FETCH_PRODUCTS_BY_ENTERPRISE,
   DID_FETCH_PRODUCTS_BY_ENTERPRISE,
 } from './actionType';
/*************
 * Constants *
 *************/
const initialState = {
  productsByEnterprise: [],
  fetchProductsByEnterpriseIsBeingExecuted: false,
  statusOfFetchingProductsByEnterprise: {
    wasError: false,
    error: null,
  },
};
/**
 * It manages the products fetch process
 */
export default function fetchProductsByEnterprise(state = initialState, action) {
  
  switch (action.type) {

    case WILL_FETCH_PRODUCTS_BY_ENTERPRISE:
      return Object.assign({}, state, {
        fetchProductsByEnterpriseIsBeingExecuted: true,
        statusOfFetchingProductsByEnterprise: {
          wasError: false,
          error: null,
        },
      });
      
    case FETCH_PRODUCTS_BY_ENTERPRISE:
      if (action.error)
        return Object.assign({}, state, {
          statusOfFetchingProductsByEnterprise: {
            wasError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          productsByEnterprise: action.payload,
        });
        
    case DID_FETCH_PRODUCTS_BY_ENTERPRISE:
      return Object.assign({}, state, {
        fetchProductsByEnterpriseIsBeingExecuted: false,
      });
      
    default:
      return state;
  }
};