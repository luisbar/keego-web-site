/***********************
 * Node modules import *
 ***********************/
import { combineReducers } from 'redux';
/******************
 * Project import *
 ******************/
import fetchProductsByEnterpriseReducer from './fetchProductsByEnterprise/reducer';

export default combineReducers({
  fetchProductsByEnterpriseReducer,
});