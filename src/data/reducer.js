/***********************
 * Node modules import *
 ***********************/
import { combineReducers } from 'redux';
/******************
 * Project import *
 ******************/
import navigationReducer from './navigation/reducer';
import emailReducer from './email/reducer';
import productReducer from './product/reducer';

export default combineReducers({
  navigationReducer,
  emailReducer,
  productReducer,
});
