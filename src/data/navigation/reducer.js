/***********************
 * Node modules import *
 ***********************/
import { combineReducers } from 'redux';
/******************
 * Project import *
 ******************/
import animateReducer from './animate/reducer';

export default combineReducers({
  animateReducer,
});