/******************
 * Project import *
 ******************/
import {
  WILL_ANIMATE,
  DID_ANIMATE,
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  animationVisibility: true,
};
/**
 * It manages all changes on the store due to
 * animate action
 */
export default function navigation(state = initialState, action) {
  
  switch (action.type) {

    case WILL_ANIMATE:
      return Object.assign({}, state, {
        animationVisibility: false,
      });
      
    case DID_ANIMATE:
      return Object.assign({}, state, {
        animationVisibility: true,
      });
      
    default:
      return state;
  }
};