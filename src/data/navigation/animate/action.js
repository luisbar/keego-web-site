/******************
 * Project import *
 ******************/
import {
  WILL_ANIMATE,
  DID_ANIMATE,
} from './actionType.js';
/**
 * It is triggered before that a view will be pushed
 */
export default function animate() {
  
  return (dispatch) => {
    dispatch(willAnimate());
    setTimeout(() => dispatch(didAnimate()), 500);
  };
}
/**
 * It changes the state in order to know that a delay
 * will be caused before pushing a view
 */
function willAnimate() {
  
  return {
    type: WILL_ANIMATE,
  };
}
/**
 * It changes the state in order to know that a delay
 * has been caused before pushing a view
 */
function didAnimate() {
  
  return {
    type: DID_ANIMATE,
  };
}