const firebase = require('firebase');
require('firebase/firestore');
var db = firebase.firestore();

for (let i = 1; i < 11; i++) {
  db
  .collection('enterprise')
  .doc(`${i}`)
  .set({
    id: i,
    logo: `https://avatars.dicebear.com/v2/gridy/${i}.svg`,
    name: `Empresa ${i}`
  })
  .catch(error => console.log(error))
  .then(data => console.log('ok'))

  for (let j = 1; j < 5; j++)
    db
    .collection('enterprise')
    .doc(`${i}`)
    .collection('products')
    .doc(`${j}`)
    .set({
      id: j,
      description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book`,
      name: `Producto ${j}`,
      images: [
        `https://avatars.dicebear.com/v2/jdenticon/${j}.svg`
      ]
    })
    .catch(error => console.log(error))
    .then(data => console.log('ok'))
}

