/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';//To pass store to their children
import { ConnectedRouter } from 'react-router-redux';//To link the history with state
import bugsnag from 'bugsnag-js';
import createPlugin from 'bugsnag-react';
/******************
 * Project import *
 ******************/
import './theme/hpe/index.scss';
import Main from './view/main/index';
import registerServiceWorker from './registerServiceWorker';
import store, { history } from './store.js';
import { BUGSNAG_KEY } from './config';
/*************
 * Constants *
 *************/
const bugsnagClient = bugsnag(BUGSNAG_KEY);
const ErrorBoundary = bugsnagClient.use(createPlugin(React));
const firebase = require('firebase');
require('firebase/firestore');
firebase.initializeApp({
  apiKey: 'AIzaSyD4FjkBi-L32Fi_SWi_2jXq9ypk-qy7Zv4',
  authDomain: 'luisbar-210717.firebaseapp.com',
  projectId: 'luisbar-210717'
});

export default firebase.firestore();

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ErrorBoundary>
        <Main/>
      </ErrorBoundary>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
