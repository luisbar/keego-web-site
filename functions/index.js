/***********************
 * Node modules import *
 ***********************/
const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const fetch = require('node-fetch');
const cors = require('cors')({ origin: true });
const FormData = require('form-data');
/*************
 * Constants *
 *************/
const recaptchaSecret = '6LcpwmQUAAAAAPkACCEHQRuDASSm2gt4Zez-rAIc';
const recaptchaApi = 'https://www.google.com/recaptcha/api/siteverify';

const SEND_EMAIL_ERROR = 1000;
const RECAPTCHA_SERVICE_ERROR = 1001;
const NODE_MAILER_ERROR = 1002;
/**
 * Endpoint for sending an email
 */
exports.sendEmail = functions.https.onRequest((request, response) => {
  cors(request, response, function() {
    const body = request.body;

    isHumanOrBot(body.recaptchaToken)
    .then((res) => res.json())
    .then((res) => {
      if (res.success)//is human
        return sendEmail(
          body.name,
          body.email,
          body.subject,
          body.message
        );
      else
        throw {
          errorCode: RECAPTCHA_SERVICE_ERROR,
          errorInfo: res,
        };
    })
    .then((res) => response.send(res))
    .catch((err) => response.send({
      errorType: 'api',
      errorCode: err.errorCode || SEND_EMAIL_ERROR,
      errorMessage: 'Ocurrió un problema al enviar el correo, lo sentimos :(',
      errorInfo: err.errorInfo || err, 
    }));
  });
});
/**
 * It makes a request to the recaptcha API in order to check
 * if it is a human or bot who invoked the send email method
 * @param  {string}  recaptchaToken token sending for the client
 * @return {promise}
 */
function isHumanOrBot(recaptchaToken) {

  let formData = new FormData();
  formData.append('secret', recaptchaSecret);
  formData.append('response', recaptchaToken);
  
  return fetch(recaptchaApi, {
    method: 'POST',
    body: formData,
  });
}
/**
 * It sends an email
 * @param  {string} name name of user
 * @param  {string} email email of the user
 * @param  {string} subject subject of the message
 * @param  {string} message message
 * @return {promise}
 */
function sendEmail(name, email, subject, message) {

  return new Promise((resolve, reject) => {
    let transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: 'luisbar180492@gmail.com',
        pass: 'huachinango123'
      }
    });

    let mailOptions = {
      from: `${name} 👻 <${email}>`,
      to: 'luisbar180492@gmail.com',
      subject: subject,
      text: message,
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error)
          reject({
            errorCode: NODE_MAILER_ERROR,
            errorInfo: error,
          })
            
        resolve(true);
    });
  });
}
