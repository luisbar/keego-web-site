#### How to run

###### Using docker (firestore and function emulators)

- Build emulators image
```sh
docker build --pull --rm -f "Emulators" -t emulators "."
```

- In the `index.js` file into `src` folder change the following lines:
```diff
- firebase.initializeApp({
-  apiKey: 'AIzaSyD4FjkBi-L32Fi_SWi_2jXq9ypk-qy7Zv4',
-  authDomain: 'luisbar-210717.firebaseapp.com',
-  projectId: 'luisbar-210717'
- });

+ firebase.initializeApp({
+  projectId: 'test'
+ });
```

- Additionally add the following import in the previous file
```diff
export default firebase.firestore();
+ require ('./populateFirestoreEmulator.js')
```

- Build web image
```sh
docker build --pull --rm -f "Web" -t web "."
```

- Run container using emulators image
```sh
docker run -d -p 3006:3006 -p 3005:3005 emulators
```

- Run container using web image
```sh
docker run -d -p 3000:3000 web
```

- Web site will be available on http://localhost:3000

:warning: If you run npm install, please remove node_modules and package-lock.json on root and functions folders

:warning: When you run firestore emulators the following environment variable is created and used
by the function FIRESTORE_EMULATOR_HOST=0.0.0.0:3005

:warning: You configure the host and port of firestore and function emulators using the firebase.json file, and you have to put 0.0.0.0 in the host in order to avoid connection problems

###### Normal way (using functions and firestore in the cloud)

- Run
```sh
npm i && npm start
```

- Web site will be available on http://localhost:3000